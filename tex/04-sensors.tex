\chapter{Podpora EV3 senzorů} \label{chap:impl_sensors}
V~této kapitole bude popsán postup, jakým byla do Open-Cube
přidána podpora pro senzory ze stavebnice EV3.

Úlohu lze rozdělit na tři podproblémy:
\begin{itemize}
    \item Zprovoznění obecné UART komunikace na portech pro senzory.
    \item Implementace EV3 UART protokolu.
    \item Vytvoření uživatelsky přívětivého rozhraní pro všechny senzory.
\end{itemize}

\section{Postup a programovací jazyk}

Při programování potřebného kódu byl z~mé strany zvolen specifický postup:
\begin{enumerate}
  \item Nejprve byla v~sekci \ref{sec:pc-ev3-sensors} vytvořena aplikace
        pro čtení digitálních EV3 senzorů na běžném PC.
        Cílem bylo seznámit se s~jejich protokolem a vytvořit základní kostru ovladače.
        Výsledný kód lze využít pro výukové a testovací účely.
  \item Následně bylo v~části \ref{sec:pyopencube} naprogramováno vše
        potřebné pro podporu senzorů na Open-Cube za použití pouze programovacího jazyka Python.
        Výkon ovladače ale nebyl ideální.
  \item V~sekci \ref{sec:copencube} došlo k~přepsání jádra ovladače do jazyka C.
        Tím se podařilo dosáhnout potřebného zrychlení.
\end{enumerate}

\vfil
\pagebreak

\section{Zprovoznění na PC} \label{sec:pc-ev3-sensors}
Rozhraní UART je poměrně široce používané a existují pro něj převodníky do běžných počítačů.
Díky tomu lze připojit digitální EV3 senzory k~počítači a komunikovat
s~nimi přes standardní rozhraní operačního systému.

\subsection{Propojení PC -- senzor}
Pro vývoj aplikace bylo použito zapojení na obrázku \ref{fig:pc-ev3-hw}.
Pro převod mezi UART a USB byl zvolen jeden z~komerčně dostupných adaptérů.
Propojení převodníku a senzoru zajišťuje upravený LEGO kabel na obrázku \ref{fig:pc-ev3-cable}.
Kabel vznikl rozpůlením běžného propojovacího kabelu, na volný
konec pak byly umístněny dutinky pro zapojení do převodníku.

\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/legoport-pc.eps}
\caption[Připojení senzoru k~PC]{Schéma propojení EV3 senzoru s~počítačem.}
\label{fig:pc-ev3-hw}
\end{figure}

\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/pc-ev3-cable-foto.jpg}
\caption[Fotografie kabelu PC-senzor]{Praktická realizace zapojení \ref{fig:pc-ev3-hw}.}
\label{fig:pc-ev3-cable}
\end{figure}

\subsection{Výsledná aplikace}

Výstupem této části je konzolová aplikace v~Pythonu, která obsahuje
základ ovladače pro EV3 UART protokol. Jedinou funkcí aplikace je provést úvodní
komunikaci se senzorem a pak začít vypisovat data, která senzor posílá.

Blokové schéma aplikace je naznačené na obrázku \ref{fig:pc-ev3-sw}.
Další sekce popíší funkce jednotlivých vrstev.

\begin{figure}[ht]
\includegraphics[width=0.6\textwidth]{img/pc-ev3-sw.pdf}
\caption[Blokové schéma PC aplikace]{Blokové schéma aplikace pro komunikaci se senzory.}
\label{fig:pc-ev3-sw}
\end{figure}

\subsubsection{Sériový port}
Na nejnižší vrstvě se řeší ovládání převodníku a tím umožnění komunikace na úrovni protokolu UART.
Využita k~tomu byla běžně dostupná Python knihovna \verb|pySerial| \cite{pyserial}.
Ta poskytuje přívětivější rozhraní nad nízkoúrovňovými voláními operačního systému.

\subsubsection{Rozdělení datového proudu do zpráv}

Komunikace po sériové lince probíhá na úrovni jednotlivých bajtů,
EV3 protokol je ale založený na posílání vícebajtových zpráv.
Tato vrstva pomocí jednoduchého stavového automatu zařídí
rozdělení proudu bajtů na jednotlivé zprávy. Automat je naznačený na
obrázku \ref{fig:pc-frame-decoder}.

\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/frame-decoder.pdf}
\caption[Dělení datového toku na zprávy]{Stavový automat
zařizující rozdělení příchozího proudu dat na jednotlivé zprávy.}
\label{fig:pc-frame-decoder}
\end{figure}

Vrstva musí zajistit také \uv{synchronizaci} komunikace se senzorem.
EV3 protokol neposkytuje žádný jedinečný oddělovací bajt, kterým by začínala každá zpráva.
Místo toho má každá zpráva jednobajtovou hlavičku a z~ní vyplývá celková délka zprávy.
Jakmile kostka úspěšně jednu zprávu dekóduje, dokáže najít začátek následující zprávy.

Postup na dosažení synchronizace na začátku komunikace
byl nalezen ve zdrojových kódech EV3 firmwaru \cite{ev3-src}.
První zpráva vyslaná senzorem má pevný formát a obsahuje číselné ID senzoru.
Na začátku komunikace stačí čekat, dokud poslední tři přijaté bajty neodpovídají této zprávě.
Pokud ano, podařilo se navázat synchronizaci a aplikace může dekódovat další zprávy.

\subsubsection{Dekódování zpráv}
Cílem této vrstvy je překládat mezi binárním formátem zpráv a
pomocnými datovými třídami, které napomáhají lepší čitelnosti kódu.
Např. zprávu \verb|NACK| přeloží na odpovídající bajt \verb|0x40| a nebo naopak.

\subsubsection{Metadata ze senzoru}
Tato vrstva zajišťuje zpracování metadat, které senzor posílá po připojení.
Informace využívá například pro převod naměřených hodnot do jednotek SI.

\subsubsection{Řízení a zobrazování komunikace uživateli}
Úkolem této vrstvy je propojit zbylé komponenty aplikace a poskytnout
uživateli základní konzolové rozhraní. Přes něj program zobrazuje
výpis zpráv, které přijal od senzoru.

Přechod mezi inicializační a datovou fází komunikace
(viz sekce \ref{sec:ev3digital}) řídí právě tato vrstva.
Jakmile od senzoru přijdou veškerá metadata,
vydá pokyn k~odeslání potvrzovací zprávy zpět.

Dalším zodpovědností vrstvy je posílání pravidelných obnovovacích
zpráv do senzoru. Bez nich by se senzor do jedné vteřiny restartoval.
Správně by vrstva měla také sledovat, jestli senzor na tyto zprávy odpovídá.
Verze pro počítač to neprovádí, pro zjednodušení to bylo implementováno
až v~pozdější verzi na Open-Cube.

\subsubsection{Spouštění a možnosti využití}
Program lze na Linuxových operačních systémech spustit příkazem:
\begin{lstlisting}
$ python3 main.py /dev/ttyUSB0 -m <nazev_modu_senzoru>
\end{lstlisting}
Parametr \verb|/dev/ttyUSB0| je cesta k zařízení převodníku a \verb|nazev_modu_senzoru|
je režim, do kterého má aplikace senzor po navázání komunikace přepnout.
Seznam režimů a jiná metadata senzoru lze vypsat příkazem:
\begin{lstlisting}
$ python3 main.py /dev/ttyUSB0 -s
\end{lstlisting}

Kód aplikace lze v~budoucnu využít i jiným způsobem.
Jednak může sloužit jako pomocný materiál při studiu EV3 protokolu dalšími lidmi.
Další možností je přes ní testovat budoucí senzory vlastní výroby.

\section{Zprovoznění na Open-Cube}\label{sec:pyopencube}

Dalším krokem bylo za pomoci programovacího jazyka Python zprovoznit
EV3 senzory na Open-Cube.

\subsection{Zapojení sériových linek}

Nejprve bylo nutné zprovoznit komunikaci na portech pro senzory.
Jak bylo naznačeno v~sekci \ref{sec:opencube:pio},
Open-Cube na to nemá dostatečný počet hardwarových UART periferií.
Proto bylo nezbytné pro některé porty využít univerzální periferii PIO.

Obrázek \ref{fig:opencube-uart-routing} ukazuje návrh zapojení.
Přiřazení HW UART periferií je omezené tím, ke kterým pinům RP2040 je lze připojit.
Zapojení PIO periferie lze naopak volně upravovat.

Varianta zvolená níže má za cíl ušetřit instrukční paměť bloků.
\uv{SM \#0} v~obrázku naznačuje, že příjem běží na stavovém automatu 0 prvního
PIO bloku a vysílání běží na témže automatu druhého bloku.
Veškerý příjem tak probíhá v~PIO 0 a vysílání v~PIO 1.

\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/opencube-uart-routing.pdf}
\caption[Zapojení sériových linek]{Zapojení sériových linek Open-Cube.}
\label{fig:opencube-uart-routing}
\end{figure}

Při realizaci plánu se ale vyskytl problém.
Na prvním portu se nedařilo žádná data přijmout ani odeslat.
Pro testování byl opět použit převodník USB-UART, zde v~zapojení na obrázku \ref{fig:opencube-pc-link}.

\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/legoport-pc-crossed.eps}
\caption[Připojení Open-Cube k~PC]{Schéma propojení Open-Cube s~počítačem přes senzorový port.}
\label{fig:opencube-pc-link}
\end{figure}

Později bylo zjištěno, že jde o~chybu v~zapojení Open-Cube.
Naznačuje ji obrázek \ref{fig:opencube-bad-uart}.
RP2040 nedokáže namapovat příjem a vysílání hardwarové periferie tak, jak je Open-Cube zapojená.
Přijímat dokáže pouze na svém GPIO pinu 5, vysílat na pinu 4 \cite[236]{rp2040-ds}.
Schéma ovšem počítalo s~obráceným zapojením \cite{opencube-schematics}.

\begin{figure}[ht]
\includegraphics[width=0.75\textwidth]{img/opencube-bad-uart.pdf}
\caption[Chyba v~zapojení UARTu]{Znázornění prohozených UART pinů na desce plošných spojů.}
\label{fig:opencube-bad-uart}
\end{figure}

\pagebreak

Závadu se podařilo vyřešit. Problém byl z~mé strany konzultován s~autory Open-Cube.
Ti pak na použité desce provedli nezbytné prohození přijímacího a vysílacího
vodiče. Poté již komunikace fungovala.

Problém bylo možné obejít i jiným způsobem. Na obrázku \ref{fig:opencube-uart-routing}
má každý PIO UART dedikovaný přijímač a vysílač.
Přijímače nezbytně musí být oddělené -- každý senzor
vysílá nezávisle na ostatních. Vysílače kostky by ale
potenciálně mohly být sdílené. Díky flexibilitě PIO by bylo
možné jeden vysílač přepínat mezi několika porty.
Na sériové linky by se tak využilo pouze pět stavových automatů z~osmi celkových.
Toto řešení by ale zesložitilo ovladač.


\subsection{Ovladač PIO UART}

MicroPython neobsahuje zabudovanou podporu pro ovládání sériové linky přes
periferii PIO. Musela proto být doprogramována v~Pythonu.

\noindent Aby mohlo PIO fungovat jako UART, jsou k~tomu nezbytné dvě komponenty:
\begin{itemize}
  \item Stavový automat, který poběží na primitivním procesoru uvnitř PIO.
  \item Ovladač, který bude s~automatem komunikovat.
\end{itemize}

Jako základ stavového automatu byla použita ukázka
z~dokumentace RP2040 \cite[348]{rp2040-ds}.
Tyto automaty poskytují skoro vše potřebné pro UART --
vysílací automat byl použit beze změny.
Přijímací automat byl dále upraven tak, aby se snížilo
množství generovaných přerušení, což je ještě popsáno níže.

Spolu s~automatem bylo potřeba navrhnout způsob, jakým bude probíhat komunikace
mezi PIO a ovladačem. Návrh vychází ze struktury ovladače
hardwarové periferie UART v~MicroPythonu \cite{mpy-rp2-uart}. Obrázek \ref{fig:pio-uart-fifo}
ilustruje zvolené řešení.

\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/pio-uart-fifo.pdf}
\caption[Tok dat frontami PIO UARTu]{Tok dat frontami sériové linky realizované periferií PIO.}
\label{fig:pio-uart-fifo}
\end{figure}

\begin{itemize}
    \item Přijímací stavový automat bude data průběžně
          vkládat do hardwarové fronty. Jakmile se fronta
          naplní, vyvolá se přerušení na ARM jádře.
    \item Obsluha přerušení přesune data z~hardwarové
          fronty do větší softwarové fronty. Ta slouží
          pro dočasné uložení dat v~případě, že software
          na vyšších vrstvách nestíhá data zpracovávat.
          Zvolená velikost fronty odpovídá 88 milisekundám
          dat na 57600 baudech.
    \item Funkce pro vyčtení dat z~periferie nejprve zavolá sama obsluhu přerušení.
          Tím dojde k~přesunu dat z~hardwarové do softwarové fronty.
          Odtud poté vyčítací funkce odebere potřebný počet bajtů.
    \item Funkce pro vyslání dat přímo vkládá data do
          hardwarové fronty, kde si je automat vyzvedne.
          Automat tak žádná přerušení ARM jader nevytváří.
          EV3 protokol potřebuje posílat pouze minimum dat
          z~kostky do senzoru, proto lze toto zjednodušení provést.
\end{itemize}

Úprava přijímacího automatu spočívala v~přidání podpory
pro opožděné vyvolání přerušení. Původní automat pouze
vkládal data do HW FIFO. Nově automat také vyvolá přerušení na
ARM jádře v~okamžiku, kdy ve frontě zbývá místo na jediný bajt.
Myšlenka na tuto optimalizaci pochází z~ovladače hardwarové UART
periferie RP2040 \cite{mpy-rp2-uart} a v~dokumentaci RP2040 \cite[423]{rp2040-ds}.

Důvodem pro tuto úpravu je omezení režie.
V~Pythonu musela být napsána i obsluha přerušení.
To MicroPython podporuje \cite{mpy-irq}, ale s~poměrně velkou latencí \cite{mpy-irq-latency}.
Tato úprava zajišťuje, že se obsluha nevolá pro každý bajt zvlášť, ale naopak
vyzvedne blok několika bajtů najednou. To již při rychlosti
57600 baud, kterou senzory používají, může mít podstatný vliv.

Schéma \ref{fig:pio-uart-fifo} bylo následně naprogramováno pomocí
knihoven dostupných v~MicroPythonu -- např. třída \verb|rp2.StateMachine| \cite{mpy-pio-sm}
poskytuje rozhraní k~periferii PIO. Některé operace ale nebyly v~Pythonu
dostupné (např. nastavení úrovně HW FIFO, při které se vyvolá přerušení). Tyto funkce bylo možné doplnit
pomocí přímého přístupu do fyzické paměti, který
MicroPython také podporuje \cite{mpy-mem-access}.

Při programování byl pro další snížení režie použit
režim MicroPythonu pojmenovaný jako Viper \cite{mpy-speed}.
V~tomto režimu MicroPython kód neinterpretuje, ale rovnou
jej překládá do strojového kódu procesoru. Viper navíc
s~pomocí anotací dokáže některé operace převést
na jednoduché instrukce procesoru (např. sečtení dvou čísel
nebo přístup ukazatelem do paměti). \cite{mpy-speed}

Použitím Viperu bylo možné například zrychlit přístup
k~softwarové kruhové frontě. Před jeho použitím
musela její implementace používat datové položky třídy.
Hledání položek při každém přístupu ale bylo poměrně pomalé.
Pomocí Viperu bylo možné všechna data umístit do jednoho bloku
bajtů, ke kterému šlo rychle přistupovat z~obsluhy přerušení
i z~vyčítací funkce.

\subsection{Přenesení EV3 protokolu} \label{sec:pyev3proto}
Nad rozhraní pro sériovou linku již bylo možné
přesunout implementaci EV3 protokolu, která byla připravena
v~sekci \ref{sec:pc-ev3-sensors}. Kód byl dále upraven a rozšířen.
Výsledná struktura ovladače je nakreslená na obrázku \ref{fig:opencube-uart-stack}.

\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/opencube-uart-stack.pdf}
\caption[Ovladač senzorů v~Pythonu]{Celková architektura
Python řešení pro EV3 UART senzory.}
\label{fig:opencube-uart-stack}
\end{figure}

\pagebreak
Oproti řešení na počítači byly provedeny tyto změny:
\begin{itemize}
    \item Kód je celkově řešený tak, aby se co nejvíce vyhýbal alokacím
          nových objektů. Díky tomu by se měla zmenšit frekvence
          \uv{stop-the-world} pauz, které MicroPython občas musí provést při
          hledání volné paměti pro nové alokace \cite{mpy-speed}.
    \item Rozhraní pro komunikaci po sériové lince používá neblokující přístup.
          Díky tomu je možné komunikaci na portu pravidelně sledovat na pozadí uživatelského programu.
    \item Zprávy ze senzoru se zpracovávají každých 10 milisekund
          v~přerušení časovače.
    \item Algoritmus již kontroluje, jestli senzor odpovídá na
          pravidelné obnovovací zprávy (v~obrázku nazváno jako
          heartbeat). Pokud ne, ovladač se zresetuje a
          přejde opět do inicializační fáze komunikace.
\end{itemize}

Nad obecnou implementací protokolu byly vytvořeny třídy
specifické pro konkrétní senzory. Ty poskytují přívětivější
rozhraní pro studenty a další uživatele. Otestované jsou třídy
pro EV3 ultrazvukový senzor, gyroskop a senzor barev.
Třída pro infračervený senzor otestována nebyla, protože tento
senzor není dostupný ve výukové sadě EV3.

Mimo tyto třídy lze také využít obecnou třídu \verb|GenericSensor|.
Ta na základě přijatých metadat dokáže spolupracovat
s~libovolným senzorem používajícím stejný protokol.

Pomocí tohoto řešení již bylo možné naprogramovat jednoduchého robota.
Video \url{https://youtu.be/SnLtU2dOrws} (kopii lze nalézt v~příloze)
zachycuje demonstrační vozítko se sledováním čáry. Ovládání motorů
je zde zařízené pomocí dřívějších ovladačů pro NXT motory.

\subsection{Nedostatečný výkon}

Přestože představené řešení do určité míry fungovalo,
nebylo to bez kompromisů. Aby Open-Cube stíhala zpracovávat
zprávy z~většího počtu senzorů, musela být frekvence
komunikační smyčky omezena na \SI{100}{\Hz}.
Senzory přitom data mohou posílat až 1000krát za sekundu \cite[8]{ev3hdk};
přebytečné zprávy musí smyčka přeskakovat.

Opatření bylo provedeno kvůli zahlcení kostky zprávami.
Když zpracování zpráv probíhalo častěji a bez jejich přeskakování, kostka při
připojení více senzorů již hůře stíhala vykonávat uživatelský program.

Omezení na \SI{100}{\Hz} by přitom mohlo působit problémy
při vytváření soutěžních robotů. Proto bylo dále jádro ovladače přepsáno
do jazyka C. Další pokusy o~optimalizaci zpracování zpráv v~Pythonu
nebyly úspěšné.

\vfil\pagebreak

\section{Přepis do C} \label{sec:copencube}
Podpora sériové linky a implementace EV3 protokolu byly
přepsány do podoby nativních uživatelských modulů (viz sekce \ref{sec:mpy-cmods}).
Výsledná architektura ovladače je znázorněná na obrázku
\ref{fig:opencube-uart-stack-v2}.
\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/opencube-uart-stack-v2.pdf}
\caption[Ovladač senzorů v~C]{Celková architektura nativního řešení pro EV3 UART senzory.}
\label{fig:opencube-uart-stack-v2}
\end{figure}

Vrstvy pro UART a pro EV3 protokol byly umístěny do
oddělených uživatelských modulů. Dohromady poskytují jeden jmenný
prostor \verb|ev3_sensors_ll|, který zpřístupňuje jádro EV3
protokolu do Pythonu.

V~této variantě se již pro interakci s~hardwarem používá přímo RP2040 SDK \cite{rp2040-csdk}.
Kód je ale převážně strukturován tak, aby jeho vyšší vrstvy bylo možné
využít i na jiných platformách.

Komunikační smyčka se nyní spouští každou milisekundu. Open-Cube tak
dokáže od senzorů přebírat data stejně rychle jako kostka EV3 \cite[8]{ev3hdk}.

\subsection{Zhodnocení}

Pro otestování vlivu přepsání ovladače z~Pythonu do C byl navržen
jednoduchý experiment. V~něm se měří doba trvání iterace jednoduché smyčky.
K~Open-Cube byl současně připojen gyroskop a senzor barev, aby
ovladač na pozadí smyčky musel přijímat data.

Měření ideově odpovídá následujícímu kódu. Ten má simulovat
běh regulační smyčky programu na frekvenci \SI{1}{\kilo\Hz}.
\begin{lstlisting}[language=Python]
last_time = utime.ticks_us()
for index in range(4096):
    utime.sleep_us(1000)
    current_time = utime.ticks_us()
    data[index] = utime.ticks_diff(current_time, last_time)
    last_time = current_time
\end{lstlisting}

Aby oba jazyky měly srovnatelné podmínky, byla u~obou variant nastavena
frekvence komunikačních smyček na \SI{1}{\kilo\Hz} a bylo deaktivováno přeskakování zpráv.

Z~časových intervalů mezi jednotlivými běhy smyčky byl následně vytvořen histogram.
Výsledný graf pro obě varianty je na obrázku \ref{fig:py-delays}.

\begin{figure}[ht]
  \includegraphics[width=\textwidth]{img/tsbench.eps}
  \caption[Porovnání rychlosti mezi Pythonem a C]{Histogram trvání jedné iterace smyčky pro Python i C verzi.}
  \label{fig:py-delays}
\end{figure}

Je patrné, že rozptyl trvání iterací je výrazně větší u~řešení v~Pythonu.
Směrodatná odchylka pro řešení v~Pythonu je \SI{1.1}{\milli\second}, pro nativní řešení
jen \SI{6.8}{\micro\second}. Přepsání jádra ovladače do C tedy
skutečně zlepšilo jeho chování -- hlavní smyčky uživatelských programů
budou s~novou verzí mít konzistentnější časování.

\section{Finální Python API}

Třídy určené pro uživatele byly zpřístupněny ve jmenném prostoru \verb|lib.ev3|.
Kde to bylo výhodné, tam se rozhraní inspiruje prostředím Pybricks \cite{pybricks-ev3api}
které umožňuje v~Pythonu programovat kostky EV3. To by mělo zjednodušit přechod
z~řídících jednotek EV3 na Open-Cube.

\subsection{Společné metody} \label{sec:common-drv}
Třídy pro digitální EV3 senzory (ultrazvukový, infračervený, gyroskop a senzor barev) sdílejí část svého rozhraní.

\begin{itemize}
    \item Konstruktor \verb|__init__(port, wait_ms=None)| vytvoří
    nový objekt senzoru navázaný k~danému portu.

    Povinný argument \verb|port| vyžaduje zadání čísla portu. Přípustné
    hodnoty jsou Open-Cube konstanty \verb|Port.S1| až \verb|Port.S4|.

    Nepovinný parametr \verb|wait_ms| umožňuje uživateli vyčkat na inicializaci senzoru.
    Pokud senzor nezačne do dané doby komunikovat, funkce vyvolá výjimku \verb|SensorNotReadyError|.

    V~jeden okamžik může být na jednom portu aktivní pouze jeden objekt
    senzoru. Při pokusu o~vytvoření druhého objektu se vyvolá výjimka
    \verb|PortAlreadyOpenError|.

    \item Metoda \verb|close()| deaktivuje objekt senzoru
    a uvolní tak port pro použití jiným objektem.

    \item Metoda \verb|is_connected()| vrátí pravdivou hodnotu jen tehdy,
    pokud kostka detekovala libovolný EV3 UART senzor.

    \item Metoda \verb|is_ready()| zjistí, jestli senzor již posílá data.

    \item Metoda \verb|wait_for_connection(wait_ms=None)| počká
    daný čas (ve výchozím nastavení \SI{3}{\second}) na to,
    aby proběhla inicializace senzoru. Pokud se do té doby spojení nenaváže,
    vyvolá se výjimka \verb|SensorNotReadyError|.

    \item Metoda \verb|start_reset()| vyvolá restart senzoru.
    Ten je u~některých revizí gyroskopu nutný k~vyvolání
    rekalibrace, pokud začne měřený úhel driftovat od skutečné hodnoty \cite{seshan-gyro}.
\end{itemize}

\subsection{Senzor barev}
Třída \verb|lib.ev3.ColorSensor| poskytuje tyto metody navíc:
\begin{itemize}
    \item Metoda \verb|reflection()| změří v~\% odrazivost povrchu pro červené světlo.
    Kalibrace hodnot je nastavená v~senzoru a nelze ji jednoduše změnit \cite{ev3-reversing}.

    \item Metoda \verb|reflection_raw()| změří také odrazivost, ale využije k~tomu
    nekalibrovaný režim senzoru. Touto funkcí lze dosáhnout lepšího rozlišení za cenu toho,
    že uživatel musí provést vlastní kalibraci hodnot.

    \item Metoda \verb|ambient()| určí intenzitu okolního světla v~procentech.

    \item Metoda \verb|rgb_raw()| změří odrazivost povrchu pro červenou, zelenou a modrou barvu.
    Funkce vrací nekalibrovanou trojici \verb|(r,g,b)|.

    \item Metoda \verb|rgb()| provede měření podle \verb|rgb_raw()| a výsledek
    pak na Open-Cube přepočítá na procenta pomocí vlastních kalibračních hodnot.
    Výstupem je trojice \verb|(r,g,b)| obsahující odrazivosti v~procentech.

    \item Atribut \verb|rgb_calibration| obsahuje kalibrační parametry
    výstupu senzoru v~RGB módu. Výchozí kalibrační konstanty
    byly určeny na základě maximálních naměřených hodnot na jednom senzoru.

    Každá barevná složka má dvě kalibrační konstanty -- \verb|x_offset| a \verb|x_scale|
    (\verb|x| může být \verb|r|, \verb|g| nebo \verb|b|).
    Přepočet na procenta proběhne podle rovnice
    \begin{equation}
      x_\text{pct} = (x_\text{raw} - x_\text{offset}) \cdot x_\text{scale}.
    \end{equation}

    \item Metoda \verb|hsv()| přepočítá výstup funkce \verb|rgb()|
    do barevného prostoru HSV. Výstupem je trojice \verb|(h,s,v)|, kde
    odstín \verb|h| je ve stupních a sytost \verb|s| a jas \verb|v| jsou v~procentech.
    Pro přepočet se využívají vztahy odvozené ve zdroji \cite{hsv-src}.

    \item Metoda \verb|color()| přepne senzor do režimu určování
    barvy LEGO kostičky a vrátí výsledek tohoto měření. Pokud
    se barvu nepodařilo identifikovat, vrátí funkce \verb|None|.
    Jinak vrátí jedno z~čísel barvy z~\verb|lib.ev3.Color|:
    \verb|BLACK|, \verb|BLUE|, \verb|GREEN|, \verb|YELLOW|, \verb|RED|,
    \verb|WHITE|, nebo \verb|BROWN|.
\end{itemize}


\subsection{Gyroskop}
Třída \verb|lib.ev3.GyroSensor| rozšiřuje rozhraní takto:
\begin{itemize}
    \item Metoda \verb|angle()| vrátí relativní úhlovou polohu senzoru.
    \item Metoda \verb|speed()| změří úhlovou rychlost senzoru ve stupních
    za sekundu. Měřitelný rozsah rychlostí je \SI{\pm440}{\degree/\second}
    \cite{ev3dev-sensordata}.
    \item Metoda \verb|angle_and_speed()| vrátí dvojici \verb|(angle,speed)|
    obsahující současnou úhlovou polohu a rychlost senzoru. Tato metoda
    je výhodnější než kombinace \verb|angle()|+\verb|speed()|, protože
    při jejím použití se nemusí pravidelně přepínat režim senzoru.
    \item Metoda \verb|coarse_speed()| změří úhlovou rychlost senzoru
    s~větším rozsahem. Podle \cite{ev3dev-sensordata} je rozsah hodnot
    v~tomto režimu senzoru přibližně \SI{\pm1400}{\degree/\second}.
    \item Metoda \verb|tilt_speed()| změří rychlost otáčení senzoru
    podél druhé osy ve stupních za sekundu. Tento režim funguje pouze
    na některých revizích gyroskopu -- ne všechny obsahují dvouosé snímače \cite{ev3dev-sensordata}.
    \item Metoda \verb|tilt_angle()| vrátí relativní úhlovou polohu senzoru
    podél druhé osy. Tento režim je opět přítomný jen u~některých senzorů \cite{ev3dev-sensordata}.
    \item Metoda \verb|reset_angle(angle=0)|
    umožňuje nastavit úhlový offset pro funkce
    \verb|angle()|, \verb|angle_and_speed()| a \verb|tilt_angle()|.
    Metodu je potřeba zavolat až po těchto funkcích.
    Po jejím provedení začne měření příslušného úhlu vracet hodnotu
    \verb|angle|. Přepnutí senzoru do jiného režimu offset resetuje.
\end{itemize}

\subsection{Infračervený senzor}
Třída \verb|lib.ev3.InfraredSensor| zpřístupňuje infračervený senzor.

\begin{itemize}
    \item Metoda \verb|distance()| změří v~procentech relativní vzdálenost senzoru
    od nejbližšího povrchu \cite{pybricks-ev3api}.
    \item Metoda \verb|seeker(channel)| vrací dvojici \verb|(heading, distance)|
    obsahující směr a vzdálenost k~dálkovému ovladači na daném kanálu \cite{ev3dev-sensordata}.
    \item Čtení tlačítek ovladače tato třída zatím nepodporuje.
\end{itemize}

\subsection{Ultrazvukový senzor}
Podporu ultrazvukového senzoru zajišťuje třída \verb|lib.ev3.UltrasonicSensor|.
\begin{itemize}
    \item Metoda \verb|distance(silent=False)| změří vzdálenost senzoru
    od nejbližší překážky v~milimetrech. Pokud je argument \verb|silent| pravdivý,
    senzor provede měření pouze jednou a pak se uspí.
    \item Metoda \verb|presence()| umožňuje zdetekovat, zda-li je v~okolí
    senzoru aktivní ještě jiný ultrazvukový senzor.
\end{itemize}

\subsection{Senzor dotyku}
Třída \verb|lib.ev3.TouchSensor| zpřístupňuje senzor dotyku. Dostupné
jsou pouze tyto metody:
\begin{itemize}
    \item Konstruktor \verb|__init__(port)| slouží k~inicializaci této třídy.
    Povinný argument \verb|port| akceptuje jednu z~konstant \verb|Port.S1| až \verb|Port.S4|.
    \item Metoda \verb|pressed()| zjistí, zda-li je tlačítko stisklé.
    Pokud ano, vrátí \verb|True|.  Kontrola probíhá prostým přečtením příslušného GPIO pinu.
\end{itemize}


\vfil\pagebreak
\subsection{Nízkoúrovňové rozhraní pro UART senzory}
Pro ovládání digitálních senzorů lze využít také obecnou třídu \verb|lib.ev3.EV3UartSensor|.
Nad ní jsou postavené i třídy standardních senzorů.

\noindent K~dispozici jsou kromě metod ze sekce \ref{sec:common-drv} také tyto metody:
\begin{itemize}
    \item Konstruktor \verb|__init__(port, wait_ms=None, proper_id=None)|
    přijímá dodatečný argument \verb|proper_id|. Ten se využívá ke kontrole,
    že je k~řídící kostce připojený správný senzor. Pokud v~úvodu komunikace
    senzor pošle jiné ID, vyvolá se výjimka \verb|SensorMismatchError|.

    \item Metodou \verb|sensor_id()| lze získat ID senzoru, které
    kostka přijala při inicializaci komunikace. Pokud není připojený
    žádný senzor, vrací funkce \verb|None|.

    \item Metoda \verb|start_set_mode(mode)| vyšle do senzoru
    žádost o~změnu módu senzoru. Takto lze například přepnout
    barevný senzor z~měření odrazivosti na měření intenzity okolního
    osvětlení.

    \item Metoda \verb|write_command(buffer)| pošle
    do senzoru speciální servisní rámec, jehož obsah uživatel
    specifikuje argumentem \verb|buffer|. Tato metoda není určena pro
    běžné použití. Lze pomocí ní například vyvolat tovární kalibrační
    proceduru u~senzoru barev \cite{ev3-reversing}.

    \item Metoda \verb|read_raw()| vrací dvojici \verb|(mode, data)| obsahující
    poslední data, která senzor poslal.
    Položka \verb|mode| určuje režim senzoru, ze kterého data pocházejí.
    Proměnná \verb|data| obsahuje pole hodnot, které senzor vrátil.

    Při použití této metody je nutná obezřetnost. Vrácená data jsou platná
    pouze do dalšího volání funkce \verb|read_raw()| nebo \verb|read_raw_mode()|.
    Poté se obsah pole změní. Díky této optimalizaci ale
    není potřeba vytvářet kopii dat pro každé volání.

    \item Metoda \verb|read_raw_mode(mode)| zajistí, že senzor je
    v~režimu určeném argumentem \verb|mode|. Poté vrátí pole hodnot,
    které senzor naměřil.

\end{itemize}

Ve jmenném prostoru jsou dostupné také tři typy výjimek.
\begin{itemize}
    \item \verb|SensorNotReadyError| odpovídá situaci, že je senzor odpojený (např. při pokusu o~čtení dat).

    \item \verb|PortAlreadyOpenError| signalizuje, že uživatel se pokusil
    na jednom portu inicializovat dva různé senzory.

    \item \verb|SensorMismatchError| upozorňuje, že uživatel zapojil do
    kostky jiný senzor, než který požadoval kód programu.
\end{itemize}
