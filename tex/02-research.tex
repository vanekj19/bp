\chapter{Úvodní přehled} \label{chap:research}

\section{Kostka Open-Cube} \label{chap:opencube}

\subsection{Základní informace} \label{sec:opencube:intro}

Open-Cube již byla krátce představena v~úvodu.
Na obrázku \ref{fig:opencube-vs-ev3} je nová kostka porovnaná
s~řídící jednotkou sady EV3. Obě kostky mají podobný přístup k~ovládání a
lze je připevnit k~robotům postaveným z~LEGO Technic.

\begin{figure}[hb]
\includegraphics[width=0.9\textwidth]{img/opencube-vs-ev3.jpg}
\caption[Open-Cube porovnaná s~EV3]{Nová kostka Open-Cube (vlevo) a starší jednotka EV3 (vpravo).}
\label{fig:opencube-vs-ev3}
\end{figure}

Pro připojení k~dalším zařízením poskytuje Open-Cube tato rozhraní:
\begin{itemize}
    \item Čtyři porty pro připojení NXT a EV3 motorů.
    \item Čtyři porty pro připojení analogových NXT
          senzorů a libovolných EV3 senzorů.
    \item Jeden port pro připojení NXT ultrazvukového senzoru.
    \item USB port pro připojení hlavního procesoru kostky k~PC.
    \item Mini USB port pro programování zabudovaného modulu pro bezdrátovou komunikaci.
\end{itemize}

Bezdrátovou komunikaci Open-Cube podporuje přes Bluetooth a
v~budoucnu potenciálně též přes Wi-Fi. Pro tento účel má kostka
zabudovaný modul ESP32. \cite{opencube-web}

Oproti jednotkám NXT a EV3 má Open-Cube jinak řešené napájení.
Dřívější řídící kostky závisely na vyměnitelných bateriích nebo
externích Li-ion akumulátorech. Nová kostka má akumulátor i s~nabíjecími
obvody zabudovaný pod krytem, viz obrázek \cite{opencube-schematics}. \cite{opencube-web}

\begin{figure}[hb]
    \centering
    \includegraphics[width=0.9\textwidth]{img/opencube-pcb-front.jpg}
    \caption[Odkrytovaná Open-Cube]{Pohled na část elektroniky pod krytem Open-Cube. \cite{opencube-web}}
    \label{fig:opencube-pcb}
\end{figure}

\subsection{Hardware} \label{sec:opencube:hardware}

Open-Cube je postavená na mikrořadiči RP2040 od
firmy Raspberry Pi \cite{opencube-web}. Tento čip
v~sobě obsahuje dvě jádra ARM Cortex-M0+ na výchozí
frekvenci 125 MHz. Z~pohledu paměti nabízí 264 kB
interní RAM a
rychlé rozhraní pro externí Flash paměť. Dále poskytuje
30 obecných pinů a řadiče pro několik komunikačních protokolů,
jako jsou například UART a I\textsuperscript{2}C. Další rozhraní je možné
implementovat pomocí tzv. PIO stavových automatů blíže popsaných v~sekci
\ref{sec:opencube:pio}. \cite{rp2040-spec}

\subsubsection{Připojení procesoru k~portům}

Propojení mezi procesorem Open-Cube a vstupy/výstupy kostky
naznačuje obrázek \ref{fig:opencube-blockdiagram}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{img/opencube-simplified-block-diagram.pdf}
    \caption[Blokové schéma Open-Cube]{Zjednodušené blokové schéma Open-Cube zachycující propojení s~jejími porty. Založeno na podrobnějším blokovém schématu z~\cite{opencube-web}.}
    \label{fig:opencube-blockdiagram}
\end{figure}

Na senzorové porty má kostka z~procesoru přivedené obousměrné sériové linky.
Ty jsou určené pro komunikaci s~EV3 senzory, které jsou blíže popsány v~kapitole \ref{sec:ev3digital}.
Jeden pin portů je také připojený k~analogově-číslicovému převodníku, ten se ale pro EV3 senzory nevyužívá.
\cite{opencube-schematics}

Ovládání motorů závisí na zabudovaných H-můstcích, které spínají vinutí motorů.
Můstky lze částečně řídit přímo z~RP2040, pro některé funkce je ale
nutné využít I\textsuperscript{2}C pin expandér. Pomocí něj autoři Open-Cube
rozšířili počet pinů, které dokáže RP2040 ovládat.
\cite{opencube-schematics}

Pro zjišťování polohy motorů se počítá s~použitím kvadraturních inkrementárních čidel polohy.
Blíže jsou popsaná  v~sekci \ref{sec:ev3motor}. Jejich odrušený výstup vede přímo do RP2040 \cite{opencube-schematics}.
Při další práci bude nezbytné naprogramovat jejich softwarové dekódování,
protože RP2040 k~tomu neposkytuje jinou vhodnou hardwarovou periferii.

\subsubsection{Bloky programovatelného I/O} \label{sec:opencube:pio}

Předchozí kapitola zmiňuje, že do RP2040 vedou od portů
čtyři sériové linky. RP2040 přitom ale obsahuje pouze dvě hardwarové
UART periferie a jenom jedna z~nich je připojitelná k~senzorovému
portu \cite{opencube-schematics}\cite[13]{rp2040-ds}.

S~touto komplikací se počítalo již při návrhu Open-Cube.
Řešením je použít tzv. PIO (programmable I/O) periferii uvnitř RP2040.
Na tu lze pohlížet jako na sadu programovatelných posuvných registrů.
Jedním z~jejich cílů je právě flexibilní implementace různých sériových nebo
paralelních rozhraní \cite{rp2040-ds}.

Blokové schéma jednoho PIO bloku ukazuje obrázek \ref{fig:opencube-blockdiagram}.
RP2040 obsahuje dva nezávislé PIO bloky (PIO 0 a PIO 1) \cite[309]{rp2040-ds}.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{img/rp2040-pio.pdf}
    \caption[Blokové schéma periferie PIO]{Blokové schéma periferie programovatelného I/O. Založeno na blokovém schématu z~datasheetu RP2040 \cite[309]{rp2040-ds}.}
    \label{fig:pio-blockdiagram}
\end{figure}

Každý PIO blok v~sobě obsahuje čtyři nezávislé \uv{stavové automaty},
které ovládají přenos dat. Komunikovat se zbytkem systému mohou přes
přijímací a vysílací fronty. Mimo to mohou automaty také vyvolat přerušení
na některém z~ARM jader. \cite[310]{rp2040-ds}

Vnitřní strukturu automatů naznačuje obrázek \ref{fig:pio-sm-blockdiagram}.
Každá jednotka sestává z~přijímacího posuvného registru, vysílacího posuvného registru a
z~primitivního procesoru, který řídí přenosy dat mezi I/O frontami, posuvnými registry a obecnými vstupně-výstupními piny procesoru.
\cite[310]{rp2040-ds}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\textwidth]{img/rp2040-pio-sm.pdf}
    \caption[Blokové schéma PIO automatu]{Schéma jednoho stavového automatu. Založeno na blokovém schématu z~datasheetu RP2040 \cite[310]{rp2040-ds}.}
    \label{fig:pio-sm-blockdiagram}
\end{figure}

Programy pro tyto periferie je možné psát ve speciálním textovém jazyce,
který se pak přeloží do instrukčního kódu \cite[311]{rp2040-ds}.
Ukázky užitečných programů je možné nalézt v~dokumentaci RP2040.
Pomocí dvou stavových automatů jde například implementovat obousměrný UART \cite[348]{rp2040-ds},
pomocí jednoho automatu lze implementovat I\textsuperscript{2}C \cite[359]{rp2040-ds} nebo
SPI \cite[342]{rp2040-ds}.

\subsection{Software} \label{sec:opencube:software}

Řídící software Open-Cube je v~současnosti založený na MicroPythonu
-- interpreteru jazyka Python pro různé vestavné systémy \cite[3]{mpy-sdk}.
Pro něj již skupina studentů vytvořila základní ovladače \cite{opencube-tym}.
Umožňují například ovládání motorů, čtení analogových NXT senzorů a
snímání stavu tlačítek. Další student vytvořil menu program, pomocí
kterého lze kostku jednoduše ovládat.

Tato práce se proto bude zabývat rozšířením MicroPythonu o~knihovny,
které umožní a zjednoduší práci s~EV3 motory a senzory.

Procesor RP2040 je možné programovat také v~C/C++ pomocí oficiálního
Pico SDK \cite{rp2040-csdk}. Nad ním je postavená i výše zmíněná podpora v~MicroPythonu.
Toto SDK bylo využito při vytváření části knihoven.

\section{MicroPython} \label{chap:mpy}

MicroPython do určité míry plní roli operačního systému Open-Cube.
Poskytuje mimo jiné tyto základní služby:
\begin{itemize}
    \item \textbf{Přístup přes terminál.} MicroPython vytváří na USB virtuální
          sériový port, na kterém poskytuje běžnou Python konzoli \cite[7]{mpy-sdk}.
          Přes ni je možné řídící kostku ovládat a nahrávat na ni programy.
    \item \textbf{Systémové ovladače.} Uživatelé MicroPythonu nemusí
          znát nízkoúrovňové detaily použitého mikrořadiče. Pro mnoho
          periferií existují obecná rozhraní, například sériovou linku
          abstrahuje třída \verb|machine.UART| \cite{mpy-uart}.
    \item \textbf{Souborový systém.} MicroPython dokáže systémovou
          Flash paměť rozdělit na dvě části. V~první se nachází neměnný
          kód (dále též nazývaný jako firmware). Nad druhou oblastí
          dokáže vytvořit souborový systém. Sem je pak možné dynamicky ukládat
          uživatelské programy. \cite[12]{mpy-sdk}
\end{itemize}

\subsection{Možnosti rozšíření} \label{sec:mpyext}
Při rozšířování MicroPythonu o~další knihovny lze zvolit dva přístupy. \cite[3]{mpy-sdk} \cite{mpy-cmods}
\begin{itemize}
    \item První možností je napsat tyto knihovny v~Pythonu a nahrát je do souborového
          systému v~Open-Cube. Kód se tak nepřímo stává součástí uživatelské
          aplikace. \cite[3]{mpy-sdk}

          Výhodou tohoto přístupu je, že MicroPython firmware zůstává nezměněný.
          Díky tomu lze na Open-Cube použít standardní firmware pro Raspberry Pi Pico.
          Nevýhodou je omezení programovacího jazyka na Python a případně jednoduché C \cite{mpy-dynmods}.

          Tuto variantu dříve využívaly všechny ovladače specifické pro Open-Cube.

    \item Alternativou je zabudovat knihovny do MicroPython firmwaru.
          Tento přístup by umožnil využití plnohodnotného C a přinesl by větší volnost
          při tvorbě knihoven. \cite{mpy-cmods}\cite{mpy-dynmods}

          U~této možnosti by bylo nutné najít způsob, jak sestavit
          MicroPython firmware i s~novými knihovnami \cite{mpy-cmods}. Výsledný firmware
          by následně nahradil ten standardní pro Raspberry Pi Pico.
\end{itemize}

Při vývoji byl nejprve použit první přístup. Ukázalo se ale,
že Python není pro některé operace dostatečně rychlý. Knihovny proto
byly přepsány do C s~použitím druhého přístupu. K~tomu ale bylo nutné zjistit,
jak lze MicroPython takto rozšířit.

\subsection{Vnitřní struktura}

MicroPython je už v~základu poměrně modulární.
Na základě zdroje \cite{mpy-structure} lze jeho zdrojové kódy rozdělit přibližně takto:
\begin{itemize}
    \item Společné jádro: interpreter jazyka Python a standardní knihovna.
    \item Pomocné knihovny nezávislé na platformě, na které MicroPython běží.
    \item Knihovny specifické pro cílovou platformu, též nazývanou jako {\em port}.
\end{itemize}

MicroPython firmware lze dále rozšířit pomocí {\em uživatelských modulů}.
Pomocí nich lze vytvářet vlastní třídy a funkce viditelné
v~Pythonu, ale implementované v~C nebo jiném jazyce. \cite{mpy-cmods}

Výhoda uživatelských modulů oproti možnostem výše spočívá v~místě uložení modulů.
Jejich kód se nemusí nacházet uvnitř repozitáře MicroPythonu.
Místo toho mohou být uložené kdekoliv v~souborovém systému počítače,
plná cesta k~nim se předá až skriptům pro sestavení MicroPythonu. \cite{mpy-cmods}

Uživatelské moduly se zabudovávají do firmware během jeho sestavení.
Mimo to ale MicroPython podporuje i načítání nativního kódu za běhu.
Tento mechanismus má ale určitá omezení. Uvnitř dynamicky načtené nativní
knihovny je dostupná pouze část vnitřních MicroPython funkcí. \cite{mpy-dynmods}

U~dynamických modulů jsem se také obával problémů s~kompatibilitou
s~RP2040 SDK. Součástí SDK je poměrně složitý sestavovací systém \cite[9]{rp2040-csdk} a
do něj by bylo nutné integrovat nástroje pro vytváření dynamických modulů \cite{mpy-dynmods}.
Tomu jsem se chtěl vyhnout, proto se práce dále zaměřuje na standardní moduly.

\subsection{Sestavení pro RP2040} \label{subsec:mpy-build}

Ze struktury MicroPythonu vyplývá i způsob jeho sestavování.
Vždy je nutné sestavit konkrétní port -- v~případě Open-Cube
port \verb|rp2|. Logika uvnitř portu zařídí, že se spolu propojí
vše potřebné. \cite{mpy-cmods}

Výstupem procesu je obvykle firmware ve spustitelné podobě.
Pro RP2040 vzniknou dva soubory: ELF a UF2.
Oba obsahují stejný kód, liší se ale použitím: \cite[5]{mpy-sdk}
\begin{itemize}
    \item UF2 soubor lze využít k~jednoduchému nahrání MicroPythonu do Flash paměti.
          Jedná se o~nativní formát podporovaný RP2040 boot ROM.
          Po připojení Open-Cube v~boot režimu stačí tento soubor přetáhnout
          na zavaděčem emulovaný USB disk. \cite[5]{mpy-sdk}
    \item ELF soubor se využije při ladění MicroPythonu přes debugger. \cite[5]{mpy-sdk}
\end{itemize}

Každý z~portů je postavený nad určitým sestavovacím systémem.
Pomocí jejich jazyků lze popsat, jaké soubory (a jak) se musí
zkompilovat. MicroPython v~současnosti podporuje dva systémy:
Make nebo CMake \cite{mpy-cmods}. Port pro RP2040 používá CMake \cite[5]{mpy-sdk}.

Konkrétní postup, jak lze MicroPython sestavit, se liší mezi porty.
Pro RP2040 je možné návod nalézt v~dokumentaci od Raspberry Pi \cite[4]{mpy-sdk}.

\subsection{Nativní uživatelské moduly} \label{sec:mpy-cmods}

Uživatelský modul je možné definovat jako skupinu C souborů,
která spolu tvoří knihovnu a zabuduje se dovnitř MicroPythonu.
\cite{mpy-cmods}

Pro vytvoření uživatelského modulu je potřeba napsat CMake nebo Make
popis toho, z~jakých souborů modul sestává. Toto je poměrně přímočaré
a existují k~tomu ukázkové skripty. Ve zdrojových kódech
MicroPythonu je k~dispozici ukázkový modul \cite{mpy-cexample},
ve vývojářské dokumentaci je pak tato ukázka rozebraná \cite{mpy-cmods}.

Uvnitř modulu může být definován jeden nebo více jmenných prostorů,
které si lze v~uživ. programu vyžádat standardní konstrukcí \verb|import module_name|.

Na obrázku \ref{fig:micropython-usermod} je naznačený ukázkový jmenný prostor.
Uvnitř něj je definovaná jediná třída
\verb|MyClass|, která má konstruktor a jednu metodu \verb|class_fun|.
\begin{figure}[ht]
\includegraphics[width=\textwidth]{img/micropython-usermod.pdf}
\caption[Definice jmenného prostoru]{Příklad struktury jmenného prostoru definovaného v~C.}
\label{fig:micropython-usermod}
\end{figure}

Pro popis struktury lze použít předpřipravená C makra. Například třídu
je možné definovat voláním \verb|MP_DEFINE_CONST_OBJ_TYPE()|.
Výsledný jmenný prostor lze v~MicroPythonu zaregistrovat makrem
\verb|MP_REGISTER_MODULE()|, které jej učiní viditelným pro Python kód. \cite{mpy-cexample}

Plný seznam maker lze nalézt ve zdrojových kódech MicroPythonu
v~souboru \verb|py/obj.h| \cite{mpy-src}. Podrobnější návod na vytvoření
jmenného prostoru lze najít v~dokumentaci
MicroPythonu \cite{mpy-cmods}\cite{mpy-cexample}
a také na stránce od nezávislého přispěvatele \cite{mpy-usermod}.

\section{Senzory a motory EV3} \label{chap:ev3}

Přehled periferií dostupných ve stavebnici EV3 ukazuje obrázek \ref{fig:ev3-all-sensors}.

Senzory v~setu je možné rozdělit do dvou kategorií --
analogové a digitální. Analogový přenos dat používá pouze
senzor dotyku (\uv{tlačítko}). Zbylé senzory komunikují
s~řídící kostkou digitálně. Jedná se o~ultrazvukový senzor, infračervený senzor,
gyroskop a senzor barev. \cite{ev3hdk}

Sada poskytuje také dva typy motorů -- velký a střední. Oba mají
jednotné rozhraní a liší se pouze svým vnitřním provedením. \cite{ev3hdk}

K~propojení senzorů a motorů s~řídící kostkou používá LEGO
vlastní kabely s~upravenou koncovkou RJ12. Na konektor je
vyvedeno napájení a potřebná komunikační rozhraní.
Podrobnější informace o~konektoru jsou dostupné v~hardwarové
dokumentaci EV3 \cite{ev3hdk}.
\begin{figure}[ht]
-\includegraphics[width=\textwidth]{img/lvalk-ev3.jpg}
\caption[Senzory a motory EV3]{Senzory a motory stavebnice EV3 \cite{ev3-nxt-comparison}.}
\label{fig:ev3-all-sensors}
\end{figure}

\subsection{Senzor dotyku} \label{sec:ev3analog}
Senzor dotyku je nejjednodušším senzorem ve stavebnici.
Neobsahuje žádnou elektroniku, obsahuje pouze jeden spínač a
několik rezistorů. \cite{ev3hdk}

Zapojení portu tlačítka je naznačené na obrázku \ref{fig:legoport-touch}.
Z~něj vyplývá způsob, jak řídící kostka může zjistit stav tlačítka.
Stisknuté tlačítko vynutí na pinu 6 vysokou logickou úroveň.
Při jeho uvolnění bude pin 6 na tlačítku odpojený.
V~EV3 kostce je ale na pinu 6 slabý pull-down
rezistor, díky kterému zde kostka přečte
nízkou logickou úroveň. \cite{ev3hdk}
\begin{figure}[ht]
\includegraphics[height=8em]{img/legoport-touch-v2.eps}
\caption[Zapojení EV3 tlačítka]{Funkce pinů v~konektoru dotykového
senzoru. Konektor má stejné pořadí pinů na kostce i na senzoru,
schéma je tedy pro obě strany stejné.}
\label{fig:legoport-touch}
\end{figure}

\subsection{Digitální senzory} \label{sec:ev3digital}
Zbývající senzory jsou komplexnější. Každý z~nich obsahuje mikrořadič
řady STM8S, který realizuje většinu funkcí senzoru.
Komunikace mezi senzorem a kostkou probíhá po pinech 5 a 6
pomocí protokolu UART, jak ilustruje obrázek \ref{fig:legoport-uart}. \cite{ev3hdk}

\begin{figure}[ht]
\includegraphics[height=7em]{img/legoport-uart-v2.eps}
\caption[Zapojení EV3 UART senzoru]{Funkce pinů v~konektoru UART senzoru.}
\label{fig:legoport-uart}
\end{figure}

Nad tímto kanálem LEGO používá svůj vlastní protokol vyšší úrovně.
Ten je společný pro všechny digitální EV3 senzory \cite{ev3hdk}.
Protokol nejspíše nemá oficiální název; lze jej ale dohledat
jako \uv{EV3 UART Sensor Protocol} \cite{ev3-proto-lejos}.

Účelem protokolu je zvýšit univerzálnost senzorů. Každý z~LEGO senzorů
poskytuje několik měřících režimů -- u~senzoru barev se jedná například o~režimy
měření odrazivostni nebo okolního světla. EV3 UART protokol jednotně definuje,
jak probíhá výměna naměřených dat a jak lze senzor přepnout na jiný měřící režim.
\cite[8]{ev3hdk}

Protokol také definuje způsob, jakým senzor dokáže řídící kostku informovat
o~jeho vlastnostech. Mezi ty patří typ senzoru, podporované režimy měření,
přenosové rychlosti a formát naměřených dat. Na základě těchto metadat
dokáže řídící jednotka spolupracovat i se senzorem, který nebyl vyrobený firmou LEGO.
\cite[8]{ev3hdk}

Průběh komunikace se senzorem lze rozdělit na dvě hlavní fáze: inicializační a datovou.
Po svém připojení začne senzor opakovaně posílat řídící kostce metadata o~sobě.
Přenos dat se zahájí až ve chvíli, kdy kostka senzoru potvrdí, že
metadata úspěšně přijala. \cite[8]{ev3hdk}

Protokol obsahuje také pojistku proti kolapsu komunikace. V~datové fázi
musí řídící jednotka minimálně každých \SI{300}{\milli\second} poslat senzoru
obnovovací zprávu. Pokud ji senzor z~libovolného důvodu včas nepřijme, dohlížecí
obvod v~mikrořadiči způsobí jeho restart. \cite[8]{ev3hdk}

Oficiální dokumentaci protokolu je možné nalézt v~hardwarové dokumentaci EV3 \cite{ev3hdk}.
Ta obsahuje ale pouze vysokoúrovňový popis protokolu.
Podrobnější popis existuje v~dokumentaci alternativního firmware leJOS EV3 \cite{ev3-proto-lejos}
nebo ve zdrojových kódech továrního firmware EV3 kostky \cite{ev3-src}.
Další informace je možné získat z~implementace protokolu na straně senzoru,
která byla zdokumentována v~práci \cite{ev3-reversing}.

\subsection{Motory} \label{sec:ev3motor}
Poslední nepopsanou periferií jsou servomotory. Z~pohledu komunikace se
nacházejí na pomezí mezi digitálním a analogovým přenosem dat.

Ovládání motoru je řešené přímočaře. Uvnitř krytu se nachází
stejnosměrný motor \cite{dlech-motor}, jehož vinutí je připojené na
piny 1 a 2 příslušného portu \cite{ev3hdk}.

LEGO motory poskytují řídící kostce také informace o~jejich otáčení.
Na pinech 5 a 6 jsou vyvedené výstupy polohového čidla, viz
obr. \ref{fig:legoport-motor}. \cite{ev3hdk}

\begin{figure}[ht]
\includegraphics[height=7em]{img/legoport-motor-v2.eps}
\caption[Zapojení EV3 motoru]{Funkce pinů v~konektoru motoru.}
\label{fig:legoport-motor}
\end{figure}

Signály čidla odpovídají výstupům kvadraturního inkrementálního snímače.
Ideálním výstupem jsou dva obdélníkové signály fázově posunuté o~\SI{90}{\degree}.
Z~jejich průběhu lze určit rychlost a směr otáčení motoru.
Počítáním hran signálu lze také určit relativní polohu motoru
vůči referenční pozici.

Pro ověření vlastností snímače byly pomocí logického analyzátoru změřeny
průběh signálů. Obrázek \ref{fig:motor-enc} ukazuje výřez průběhů
z~velkého EV3 motoru, který se otáčel poměrně vysokou rychlostí.

Rozlišení polohového čidla v~EV3 motorech je poměrně nízké -- na jednu
otočku připadá pouze 180 náběžných hran jednoho signálu. Toto potvrzuje
i dekódování signálu v~EV3 firmwaru \cite{ev3-src}. Zároveň čidlo
nemá oba výstupy fázově posunuté o~přesně $90^{\circ}$, což může
komplikovat výpočet rychlosti motoru.

\begin{figure}[hb]
\includegraphics[width=0.8\textwidth]{img/lmotor-enc-v2.png}
\caption[Signály z~enkodéru motoru]{Průběh signálů z~polohového snímače.}
\label{fig:motor-enc}
\end{figure}
