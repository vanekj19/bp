\chapter{Sestavování MicroPythonu na FEL GitLabu} \label{chap:gitlab}
Použití nativních modulů s~sebou přineslo určité komplikace.
Jak bylo naznačeno v~sekci \ref{sec:mpyext}, s~nimi již není možné na Open-Cube
dále využívat standardní MicroPython firmware pro Raspberry Pi Pico.
Bylo tedy nutné vyřešit problém toho, kde sestavovat a jak distribuovat
MicroPython firmware obsahující také nové Open-Cube knihovny.

Pro tento účel byl využit FEL GitLab -- fakultou provozovaná platforma pro
spolupráci a vývoj software. Na té v~současné době probíhá vývoj Open-Cube.
GitLab mimo správy zdrojových kódů ale poskytuje také mechanismus,
jak automaticky sestavovat aplikace po přidání nových změn \cite{gitlab-ci}.

Na základě předchozích zkušeností autora a dokumentace
GitLabu \cite{gitlab-ci} bylo navrženo a realizováno řešení na obrázku
\ref{fig:micropython-gitlab}.

\begin{figure}[htb]
    \centering
    \includegraphics[width=\textwidth]{img/micropython-gitlab.pdf}
    \caption[Sestavení MicroPythonu na GitLabu]{Sestavení MicroPythonu na FEL GitLabu}
    \label{fig:micropython-gitlab}
\end{figure}

\section{Volba serveru}
Nejprve bylo nutné najít server, na kterém by se mohl MicroPython sestavovat.
Server by pak šlo ke GitLabu připojit pomocí tzv. runneru.
Runner je služba běžící na serveru, která zde umožní GitLabu spouštět různé úlohy.
\cite{gitlab-ci}

Původním plánem bylo využít servery fakulty. Ta poskytuje několik GitLab runnerů pro drobné úlohy.
Dané servery ale neměly dostatek volného místa na disku pro potřebné nástroje,
od jejich využití proto bylo dočasně upuštěno.
Místo toho byl využit soukromý server autora postavený na desce Odroid-HC2 \cite{odroid-hc2}.
Na něj byl podle dokumentace \cite{gitlab-ci} nainstalován runner a tak byl připojen k~FEL GitLabu.

Toto řešení je nešťastné v~tom, že prostředí váže na soukromý hardware.
Z~dlouhodobého hlediska by bylo lepší runner nainstalovat na server vlastněný fakultou nebo katedrou měření.
Další kroky proto směřují k~tomu, aby server bylo možné později jednoduše vyměnit.

\section{Prostředí pro sestavení MicroPythonu} \label{sec:mpyenv}
Pro sestavení MicroPythonu je potřeba několik nástrojů:
přinejmenším CMake a kompilátor pro RP2040 \cite[5]{mpy-sdk}.
Obojí je nutné na server nainstalovat.

Pro jednodušší přenositelnost bylo využito tzv. kontejnerů a jejich
implementaci jménem Docker. Tato technologie umožňuje spouštět programy
v~definovaném a izolovaném prostředí \cite{docker-intro}. GitLab ji
dokáže využít pro sestavování projektů \cite{gitlab-ci-docker}.

Obsah prostředí lze definovat pomocí textového souboru \verb|Dockerfile|
\cite{docker-file}. Společnost vyvíjející Docker ale také
poskytuje repozitář s~komunitou předpřipravenými obrazy prostředí
-- Docker Hub \cite{docker-hub}. Zde byl nalezen již hotový obraz
s~nástroji pro sestavení MicroPythonu pro RP2040 \cite{docker-mpy}.

Obraz \cite{docker-mpy} ale nebylo možné použít přímo, spustit se totiž
dokáže pouze na procesorech s~architekturou x86-64.
Použitý server ale využívá procesor architektury ARM \cite{odroid-hc2}.
Byl proto vytvořen s~ní kompatibilní klon obrazu \cite{docker-mpy}.
Jeho textová definice byla vložena do repozitáře Open-Cube.

Díky využití kontejnerů by mělo jít jednoduše změnit server,
kde sestavení probíhá. Nezbytné je pouze přenastavit GitLab na používání
obrazu \cite{docker-mpy} (viz dále) a pak nastavit, aby se úlohy odesílaly
na runner na novém serveru. Stažení obrazu kontejneru se provede automaticky.

\section{Nastavení FEL GitLabu} \label{sec:gitlabcnf}
Dalším krokem bylo nastavit FEL GitLab tak, aby v~prostředí \ref{sec:mpyenv}
vyvolal sestavení MicroPython firmware po každé změně v~repozitáři.

Nastavení bylo provedeno pomocí souboru \verb|.gitlab-ci.yml| v~repozitáři projektu.
Soubor definuje, co a jak se má sestavit a jaké jsou výstupní soubory. \cite{gitlab-ci-yml}
Definice kroků sestavení vychází dokumentace k~Pythonu na RP2040 \cite[5]{mpy-sdk}.

Výsledkem sestavení jsou dva UF2 soubory:
\begin{itemize}
    \item \verb|opencube_micropython_without_pylibs.uf2| obsahuje
    pouze MicroPython a nativní Open-Cube knihovny.
    \item \verb|opencube_micropython_with_pylibs.uf2| obsahuje
    navíc Open-Cube knihovny v~jazyce Python. Tento soubor je dále popsán v~sekci \ref{sec:mpy-pkg}.
\end{itemize}

Soubory bylo zprvu poměrně obtížné v~rozhraní GitLabu najít.
Řešením bylo na úvodní stránku dokumentace vložit dynamický odkaz
na nejnovější dostupný firmware, viz obrázek \ref{fig:micropython-readme}.
\begin{figure}[htb]
    \centering
    \fbox{\includegraphics[width=0.9\textwidth]{img/mpy-readme.png}}
    \caption[Odkaz na firmware v~README]{Odkazy na sestavený firmware v~README souboru Open-Cube.}
    \label{fig:micropython-readme}
\end{figure}

Firmware si z~této stránky může kdokoliv stáhnout a na Open-Cube nainstalovat.

\section{Vytvoření jednotného MicroPython balíčku} \label{sec:mpy-pkg}

Jak bylo uvedeno v~sekci \ref{sec:mpyext}, dříve nebyly Open-Cube Python
knihovny součástí firmwaru. Naopak je bylo nutné do kostky nahrát jako soubory.
Nativní knihovny se v~tomto liší, jsou již do firmware zabudované.
To přináší určité výhody, mimo jiné jednodušší instalaci a aktualizaci knihoven.

MicroPython nicméně poskytuje podporu i pro vestavění Python souborů.
Jejich data mohou být ve firmware uložená v~předzpracované podobě, která šetří
místo v~paměti a rychleji se načítá. \cite{mpy-manifest}

Funkci lze aktivovat pomocí tzv. manifest souboru. Přes něj je možné specifikovat,
které Python soubory se do firmware mají vložit a v~jaké podobě. \cite{mpy-manifest}
Pro Open-Cube byl příslušný soubor vytvořen tak, aby se do MicroPythonu zabudovaly
Open-Cube knihovny, menu a zároveň pomocné soubory interní pro MicroPython.

Vestavění Python knihoven bylo následně zprovozněno i na GitLabu.
Stažením a instalací souboru \verb|opencube_micropython_with_pylibs.uf2|
lze na Open-Cube nahrát nový firmware se všemi potřebnými knihovnami.
Tím se zjednodušil postup aktualizace firmware.
