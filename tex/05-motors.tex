\chapter{Podpora EV3 motorů} \label{chap:impl_motors}

\section{Cíle}

Motory lze podporovat na různé úrovni abstrakce. Na základě srovnání
s~jinými prostředími pro programování EV3 (leJOS EV3, ev3dev, Pybricks)
bylo navrženo toto rozdělení:

\begin{itemize}
    \item \textbf{Abstrakce hardware kostky}. Na této úrovni lze pouze
    řídit, jaké střední napětí bude Open-Cube do motoru posílat.
    Lze také zjistit, jaká je aktuální poloha (a příp. rychlost) motoru.

    Příkladem je třída \verb|UnregulatedMotor| z~prostředí leJOS \cite{lejos-motor-unregulated}.

    \item \textbf{Regulace rychlosti nebo polohy}. Zde již software poskytuje
    rozhraní pro udržování stále úhlové rychlosti nebo polohy motoru. To lze
    zařídit pomocí zpětnovazebních regulátorů.

    V~rámci týmového projektu na Open-Cube vznikly Python knihovny, které
    poskytují pouze regulátory a nadřazené funkce již nikoliv \cite{opencube-tym}.
    Příkladem obdobné funkcionality na EV3 je metoda \verb|Motor.run(speed)|
    existující v~Pybricks \cite{pybricks-ev3api}.

    \item \textbf{Inteligentní řízení rychlosti a zrychlení}.
    Při pohybu robota po hrací ploše může být výhodné omezit jeho rychlost
    a zrychlení. Jednou z~možností, jak to zařídit, je vygenerovat rovnoměrně
    zrychlenou trajektorii mezi polohami motoru a tu pak pomocí regulátoru sledovat.

    Tyto funkce jsou v~programovacích prostředích obvykle dostupné.
    Příkladem je funkce \verb|BaseRegulatedMotor.rotateTo(angle)|
    prostředí leJOS \cite{lejos-motor-regulated} nebo funkce \verb|Motor.run_target(speed, angle)|
    v~Pybricks \cite{pybricks-ev3api}.

    \item \textbf{Řízení celého robota}. Pro dvoukolová vozítka lze
    rozhraní dále zobecnit na povely pro celého robota. Rozhraní pak
    poskytuje například funkce pro pohyb po přímce nebo po oblouku.

    Příkladem existující implementace je třída \verb|Chassis|/\verb|WheeledChassis|
    v~leJOS \cite{lejos-motor-chassis} nebo třída \verb|DriveBase|
    v~Pybricks \cite{pybricks-motor-chassis}.
\end{itemize}

Open-Cube již základní ovladače pro motory obsahuje \cite{opencube-tym}. Cílené byly
na NXT motory, které jsou s~EV3 motory kompatibilní.
Při pokusech se ale ukázalo, že jejich kód není plně spolehlivý --
software kostky se při jejich použití občas zasekl.

Během vývoje se také ukázalo, že by bylo vhodné aspoň nejnižší vrstvu
abstrakce napsat v~jazyce C. Paralelně probíhající bakalářská práce totiž cílí
mimo jiné na přepis existujících knihoven do nativních modulů.
Změna se dotkla i některých nízkoúrovňových komponent, které jsou pro
řízení motorů nezbytné.

Z~obou důvodů byla podpora pro motory naprogramována odznova.

Na základě konzultace s~vedoucím bylo rozhodnuto, že pro naplnění
zadání BP je nutné implementovat pouze nejnižší vrstvu abstrakce.
Během přípravy testovacích robotů (sekce \ref{chap:roboti}) se ale ukázalo,
že program robotů by zjednodušila dostupnost některých pokročilejších funkcí.
Původní řešení proto bylo rozšířeno o~kód pro řízení rychlosti a zrychlení.


\section{Abstrakce hardware kostky} \label{sec:motorhwlib}

Cílem této vrstvy je skrýt detaily vnitřního zapojení Open-Cube, které je naznačené na obrázku \ref{fig:motor-hw}.

\begin{figure}[ht]
\includegraphics[width=\textwidth]{img/motor-hw.pdf}
\caption[Zapojení motorů]{Schéma připojení jednoho motoru k~Open-Cube. I\textsuperscript{2}C
pin expander je sdílený všemi porty pro motory. H-můstek má každý port vlastní.}
\label{fig:motor-hw}
\end{figure}

\noindent Z~obrázku \ref{fig:motor-hw} vyplývají některé kroky nutné k~vytvoření vrstvy.
\begin{enumerate}
    \item Nejprve je nutné zprovoznit I\textsuperscript{2}C komunikaci s~použitým
          pin expanderem.
    \item Dále je třeba nakonfigurovat PWM periferii uvnitř RP2040.
    \item Ke zjištění polohy motoru je nutné sledovat signály
          z~čidla v~motoru.
    \item Pro potřeby regulace je vhodné dále určit aktuální rychlost motoru.
          To nemusí být triviální -- enkodér má poměrně
          nízké rozlišení a tak jednoduché přístupy nemusí dávat
          dobré výsledky.
\end{enumerate}

\subsection{I\textsuperscript{2}C pin expander}
Pomocí I\textsuperscript{2}C expandéru PCF8575 lze v~Open-Cube ovládat dva vstupy použitých H-můstků \cite{opencube-schematics}:
\begin{itemize}
    \item Deaktivační pin \verb|PS| (power-save). Pomocí něj lze ovládat, zda-li je vinutí připojené k~můstku \cite{hbridge}.
    V~odpojeném stavu lze motorem volně otáčet, což může být žádoucí.
    \item Řídící pin \verb|INB|. Změnou jeho úrovně lze nastavit směr spínání H-můstku a tak směr otáčení motoru \cite{hbridge}.
\end{itemize}

PCF8575 je v~Open-Cube připojený i k~jiným periferiím.
Používá se pro čtení uživatelských tlačítek a na ovládání napájecího zdroje. \cite{opencube-schematics}

Pro tento obvod bylo nutné využít ovladač, který používá zbytek řídícího software.
Aktuální verze Open-Cube využívá ovladač od V. Jelínka, který
se v~paralelně běžící bakalářské práci zaměřuje na jiné části řídícího software.

Nejprve však bylo nutné vyřešit jednu komplikaci.

\subsubsection{Konflikty při přístupu ke sběrnici} \label{sec:i2ccrash}

Sběrnice, ke které je expander připojený, je sdílená několika zařízeními.
Zapojení naznačuje obrázek \ref{fig:i2c-bus}.
\begin{figure}[ht]
\includegraphics[width=0.4\textwidth]{img/i2c-bus.pdf}
\caption[Interní I\textsuperscript{2}C sběrnice]{Zařízení na vnitřní sběrnici Open-Cube.
Displej je problematická periferie, protože potřebuje přenášet velké bloky dat.}
\label{fig:i2c-bus}
\end{figure}

Sdílení způsobuje problémy se zablokováním
sběrnice. Nejvíce situaci komplikuje černobílý $128 \times 64$ displej.
Při přenosove rychlosti \SI{400}{\kilo\Hz} zabere jeho celé překreslení minimálně
\begin{equation}
\frac{(128 \cdot 64)~\text{px} \cdot 1~\text{bit/px}}{400000~\text{bit/s}} \approx 20~\text{ms}.
\end{equation}
Kvůli tomuto mohou vzniknout při přístupu jiným k~zařízení na sběrnici konflikty.

V~současné době ovladač displeje Open-Cube provádí přenos dat
bez zakázání přerušení \cite{opencube-display}. Může se tedy stát, že během přenosu se vyvolá
obluha přerušení, například budoucí regulační smyčka motorů.
Ta se může pokusit přistoupit k~PCF8575 pomocí stejné I\textsuperscript{2}C periferie procesoru.
RP2040 SDK tuto situaci nijak neošetřuje \cite{rp2040-i2c} a tak výsledek může být neočekávaný.

V~praxi bylo pozorováno, že přístup k~pin expanderu během překreslování
displeje způsobí vypnutí Open-Cube. Možným vysvětlením je, že po skončení
přerušení se data displeje začnou posílat do expanderu a s~určitou
pravděpodobností tak dojde k~vypnutí pinu řídícího napájecí zdroj.

Pozorovaná chyba byla z~mé strany opravena pomocí synchronizačních zámků dostupných v~RP2040 SDK \cite{rp2040-mutex}.
Před přístupem ke sběrnici se každý ovladač pokusí získat zámek pro sebe.
Je-li úspěšný, může I\textsuperscript{2}C přenos provést a pak zámek uvolnit.
Naopak, je-li zámek obsazený, nesmí ovladač RP2040 I\textsuperscript{2}C periferii
využít a musí přístup odložit na později.

\subsection{Řízení napětí na motoru}
Dále je nutné řídit vstup H-můstku \verb|INA|.
Pomocí něj lze v~Open-Cube přepínat, zda je vinutí motoru připojené k~napájení, nebo jen zkratované \cite{opencube-schematics}\cite{hbridge}.
Pokud bude procesor tento signál pulzně-šířkově modulovat, může tak řídit střední hodnotu napětí na svorkách motoru.

Pro generování takového signálu lze na RP2040 využít dedikovaný PWM (pulse-width modulation) hardware.
K~dispozici je 16 výstupů z~8 nezávislých kanálů. Zde jsou potřeba pouze
čtyři výstupy, které navíc mohou sdílet kanály díky shodné modulační frekvenci.
\cite[524]{rp2040-ds}

\noindent Příslušná periferie byla nastavena podle dokumentace přes RP2040 SDK.
\begin{itemize}
    \item Modulační frekvenci byla nastavena na 20 kHz. Tato frekvence
          je již mimo rozsah lidského sluchu, a tak nebudou motory
          vydávat pískavý zvuk.
    \item Pro rozlišení střídy byla vybrána hodnota 1 \%. Pak nejkratší
          logický pulz vytvořený RP2040 odpovídá nejkratšímu povolenému
          pulzu na vstupu H-můstku -- 500~ns \cite{hbridge}.
    \item Periferie se konfiguruje tak, aby vytvářela signál symetrický
          okolo středu jeho základní periody. Pokud je zdroj \cite[10]{Yu1997}
          interpretován správně, mělo by to vést k~nepatrnému snížení vysokofrekvenčního
          rušení při rychlých změnách střídy signálu.
\end{itemize}

Ovládání PWM periferie a expanderu PCF8575 pak bylo zpřístupněno přes
jednotné rozhraní v~C. Funkce \verb|motor_pwm_run(port, duty)|
aktivuje H-můstek a začne do motoru vysílat napětí o~dané střední hodnotě.
Napětí je zde relativní vůči napětí baterií Open-Cube.
Metoda \verb|motor_pwm_poweroff(port)| naopak způsobení odpojení a tak odbrzdění daného motoru.

\subsection{Měření polohy motoru}
Pro zjištění polohy motoru stačí dekódovat polohové signály popsané
v~sekci \ref{sec:ev3motor}. Počítáním náběžných a sestupných hran
jednoho ze signálů lze měřit okamžitou polohu motoru ve stupních.

Pro měření byl použit přístup původně implementovaný v~EV3 firmwaru \cite{ev3-src}.
Při hraně signálu na pinu 5 portu pro motory se
vyvolá obsluha přerušení. Funkce pak aktualizuje proměnnou obsahující polohu motoru:
\begin{itemize}
  \item Při náběhu pinu 5 s~aktivním pinem 6 se o~\SI{1}{\degree} sníží.
  \item Při náběhu pinu 5 s~neaktivním pinem 6 se o~\SI{1}{\degree} zvýší.
  \item Při sestupu pinu 5 s~aktivním pinem 6 se o~\SI{1}{\degree} zvýší.
  \item Při sestupu pinu 5 s~neaktivním pinem 6 se o~\SI{1}{\degree} sníží.
\end{itemize}

Teoreticky je možné dosáhnout rozlišení polohy až půl stupně \cite{dlech-enc}.
Pak procesor musel reagovat na hrany obou signálů.
Fázový posun obou signálů z~EV3 motoru ale není přesně $90^\circ$, viz obrázek
\ref{fig:motor-enc} v~sekci \ref{sec:ev3motor}. Vzdálenost mezi
dvěma kódy polohy by tak nebyla konstantní, ale měnila by se mezi cca.
$1/3$ a $2/3$ stupně \cite{dlech-enc}. Proto tato varianta nebyla implementována.


\subsection{Měření rychlosti motoru} \label{sec:motors:speed}

\subsubsection{Metoda měření}
Pro nalezení vhodné metody byla zkonzultována odborná literatura.
Přehledový článek \cite{petrella-comparison} popisuje a srovnává několik
přístupů:
\begin{itemize}
    \item Měření frekvence: rychlost odpovídá tomu, o~kolik stupňů se motor otočil během
          pevného časového intervalu. Metoda je jednoduchá na implementaci,
          trpí ale nízkou přesností při malých rychlostech. Hodnoty je možné filtrovat,
          to ale přidá určité časové zpoždění. \cite[3]{petrella-comparison}
    \item Měření periody je založené na měření intervalu mezi dvěma událostmi čidla.
          Zde by se jednalo např. o~zpoždění mezi náběžnou a sestupnou
          hranou signálu. Metoda dává dobré výsledky při nízkých rychlostech,
          je ale poměrně citlivá na neideality snímače.
          \cite[4]{petrella-comparison}
    \item Kombinovace obou metod. Tu autoři popisují také v~samostatném
          článku \cite{petrella-mixed} a krátce je popsána níže.
    \item Kalmanův filtr a Luenbergerův pozorovatel stavu: na základě modelu
          systému a příchozích měření dokáží odhadnout i neměřené
          stavy systému (zde právě rychlost).
\end{itemize}

Pro naprogramování do Open-Cube byla zvolena kombinovaná metoda.
Ta spojuje výhody měření frekvence a měření periody \cite[4]{petrella-comparison}.

Myšlenka metody je naznačená na obrázku \ref{fig:speed-msr}.
Výpočet rychlosti probíhá v~přerušení časovače s~pevnou periodou $T_s$.
Při každém přerušení algoritmus změří úhel, který se na snímači načítal
od minulého výpočtu. Rozdíl oproti měření frekvence nastává v~tom, vůči jakému
časovému intervalu se počet pulzů vztáhne. U~měření frekvence by
šlo o~periodu $T_s$. Kombinovaná metoda místo toho měří skutečný časový
interval, za jaký se pulzy načítaly. \cite[3]{petrella-mixed}

Metoda obsahuje i řešení případu, že se mezi přerušeními žádné
pulzy nenačítaly. Výpočet rychlosti se v~tom případě odloží na pozdější
přerušení, které by již pulz přijalo. Po příjmu pulzu se rychlost spočítá
pomocí celkového času od posledního přijatého pulzu. \cite[3]{petrella-mixed}

\begin{figure}[ht]
\includegraphics[width=0.8\textwidth]{img/speed-msr.pdf}
\caption[Metoda měření rychlosti]{Ilustrace zvolené metody měření rychlosti.
Inspirováno obrázkem ze zdroje \cite[3]{petrella-mixed}.}
\label{fig:speed-msr}
\end{figure}

\subsubsection{Realizace} \label{sec:motors:speed:realization}
K~realizaci výpočtu rychlosti pomocí této metody je nutné nejprve znát okamžik,
ve kterém dorazila hrana signálu na pin 5. Za tímto účelem
byla rozšířena obsluha příslušného přerušení, aby vedle polohy motoru
ukládala také aktuální hodnotu systémového časovače.

Výpočet rychlosti probíhá v~pravidelném přerušení každých 10 milisekund.
Tato frekvence je kompromisem mezi včasnou aktualizací rychlosti a mezi
jejím průměrováním při vyšších otáčkách motoru. Frekvence \SI{100}{\Hz}
by také neměla procesor znatelně zatížit.

Výsledná rychlost se ukládá do globální proměnné, která je přes
pomocnou funkci dostupná dalším knihovnám Open-Cube.


Při výpočtu se uvažují náběžné i sestupné hrany signálu.
To může zanést do rychlosti rušení, protože střída signálu není
přesně 50 \%, viz sekce \ref{sec:ev3motor}. Zjednodušuje to ale kód přerušení.
Návrh regulátorů v~sekci \ref{sec:piddesign} tuto možnou komplikaci zohlednil.

\section{Řízení pohybu motorů} \label{sec:motorctrl}

\subsection{Celková architektura}
\subsubsection{PXMC}
Návrh regulační smyčky byl inspirován knihovnou PXMC (Portable, highly eXtendable Motion Control library \cite{pxmc}).
Ta se mimo jiné používá v~robotických řídících jednotkách MARS 8 \cite{pxmc},
které jsou nasazené u~některých průmyslových robotů na fakultě \cite{rob-crs}.

Regulační smyčka PXMC při řízení stejnosměrného motoru je znázorněná
na obrázku \ref{fig:pxmc-block}. PID regulátor v~bloku \verb|do_con| zde sleduje trajektorii generovanou
blokem \verb|do_gen|. Využívá k~tomu zpětnou vazbu z~bloku \verb|do_inp|.
Motor je pak ovládaný PWM signálem v~bloku \verb|do_out|. \cite{pxmc-doc}

\begin{figure}[ht]
\includegraphics[width=\textwidth]{img/pxmc-blockchem.pdf}
\caption[Blokové schéma PXMC]{Blokové schéma regulátoru DC motoru v~knihovně PXMC. \cite{pxmc-doc}}
\label{fig:pxmc-block}
\end{figure}

Při vývoji bylo zvažováno, jestli pro řízení motorů nepoužít přímo tuto knihovnu.
Nakonec bylo rozhodnuto proti této variantě. S~knihovnou by bylo nutné
se blíže seznámit, přidat do ní podporu pro RP2040 a
integrovat ji do systému pro sestavování MicroPythonu. To bylo zhodnoceno
z~mé strany jako náročnější, než by bylo vytvoření
vlastního regulátoru specializovaného pro Open-Cube.

\subsubsection{Návrh pro Open-Cube}

Pro Open-Cube byla navržena řídící smyčka na obrázku \ref{fig:motor-pid}.
Generátor trajektorie vytváří referenci pro polohu motoru a
zároveň analyticky určuje její derivaci. Sledování reference zajišťuje
PID regulátor. Ten řídí amplitudu a polaritu napětí na vinutí motoru.
Zpětná vazba se uzavírá snímáním aktuální polohy a rychlosti motoru,
ze kterých se určí aktuální regulační odchylka. Stav motoru je monitorovaný
také generátorem trajektorie.

\begin{figure}[ht]
\includegraphics[width=\textwidth]{img/motor-pid.eps}
\caption[Blokové schéma regulátoru]{Blokové schéma navrženého regulátoru motorů.}
\label{fig:motor-pid}
\end{figure}

Pro jednodušší ladění PID regulátoru byl dále identifikován model použitých motorů.
Na jeho základě byly navrženy regulační konstanty. Posledním krokem bylo
naprogramovat potřebné pohyby do generátoru reference.

\subsection{Model motoru}
Model motoru vychází z~popisu ve zdroji \cite{ev3-motor-model}.
V~něm autoři sestavili a identifikovali stavový model velkého EV3 motoru.
Jeho vstupem je napětí na svorkách motoru, výstupem je rychlost otáčení.

Model bylo možné převést do podoby lineárního stavového modelu druhého řádu.
Při jeho analýze ale bylo zjištěno, že jeden z~jeho pólů je přibližně 60krát
rychlejší než zbývající dominantní pól. Velký EV3 motor proto lze bez velké
ztráty přesnosti modelovat také jako systém prvního řádu.
Jeho přenos pak bude mít tvar
\begin{equation}
H_\omega(s) = \frac{g}{s\tau +1},
\end{equation}
kde \(\tau\) určuje polohu pólu a \(g\) mění stejnosměrné zesílení. Pro zjištění
polohy motoru je potřeba rychlost zintegrovat, přenos se tak změní na
\begin{equation}
H_\varphi(s) = \frac{1}{s} \cdot \frac{g}{s \tau +1}. \label{eq:motor:h_varphi}
\end{equation}

\subsection{Identifikace motoru}
Dalším krokem bylo identifikovat konstanty v~přenosu \ref{eq:motor:h_varphi}.

Jejich hodnoty by bylo možné odvodit z~již zmíněného modelu velkého EV3 motoru ve zdroji \cite{ev3-motor-model}.
Autoři zde ale konstanty identifikovali pouze pro velký EV3 motor, pro střední EV3 motor konstanty chybí.
Bez dalších informací by tak bylo nutné navrhnout pro oba motory stejný regulátor.
Nebylo ale jisté, že by výsledný systém stále dobře fungoval.

Z~tohoto důvodu byla provedena nová identifikace. Postup vychází ze zdroje \cite{ev3-motor-model}.
Na velkém i středním motoru byla naměřena odezva na skok vstupu.
Daty byla poté proložena křivka, kterou by sledoval systém s~přenosem \ref{eq:motor:h_varphi}.

Pro vytvoření vstupního skoku byl použit výstup Open-Cube. Z~pohledu
vinutí motoru se jednalo o~skok z~nulového napětí do napětí baterie (v~danou chvíli o~něco více
než 7 V). Z~pohledu střídy PWM se ale jednalo o~skok z~0~\% (= 0) do 100~\% (= 1) a
vůči ní byl model dále uvažován. Konkrétní konstanty modelu se tak mohou
lišit podle aktuálního napětí baterie.

Měření polohy bylo provedeno pomocí logického analyzátoru. Pomocí
něj bylo možné s~vysokou přesností navzorkovat signály enkodéru.
Převod na polohu pak byl proveden skriptem v~prostředí Matlab.
Výsledné průběhy jsou znázorněné na obrázku \ref{fig:mstep}.

Vstupní napětí motoru při experimentu nebylo snímáno. V~další části tak bylo
nutné identifikovat také neznámé zpoždění odezvy od počátku měření.

\begin{figure}[ht]
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{img/lmotor-step.eps}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{img/mmotor-step.eps}
  \end{subfigure}
  \caption[Odezva motorů na skok napětí]{Odezva velkého a středního motoru na skok vstupního napětí.}
  \label{fig:mstep}
\end{figure}

\noindent Ideální odezva systému \ref{eq:motor:h_varphi} na skok zpožděný o~čas $T_D$ má Laplaceův obraz
\begin{equation}
\Phi(s) = e^{-s T_D} \frac{1}{s^2} \cdot \frac{g}{s \tau +1}.
\end{equation}
Provedením inverzní Laplaceovy transformace lze získat funkci
\begin{equation}
\varphi_\text{id}(t) = \mathbbm{1}(t-T_D) \cdot \left[ g \cdot (t-T_D) - g \tau + g \tau \exp \left(-\frac{t-T_D}{\tau}\right) \right]. \label{eq:motor:phiid}
\end{equation}

Pro proložení dat funkcí byla použita metoda Levenberg-Marquardt dostupná
v~optimalizační toolboxu prostředí Matlab. Pomocí ní byla minimalizována
celková chyba proložení $E$ nad všemi $N$ vzorky:
\begin{align}
E &= \sum_{k=1}^N e[k]^2, \\
e[k] &= \varphi_\text{msr}[k] - \varphi_\text{id}(t_\text{msr}[k]).
\end{align}
Za optimalizační proměnné byly zvoleny neznámé $g$, $\tau$ a $T_D$
v~rovnici \ref{eq:motor:phiid}.

\noindent Tímto postupem byly pro velký motor získány konstanty
\begin{align}
g    &\approx \SI{845}{\degree/\second}, \\
\tau &\approx \SI{0.0592}{\second}, \\
T_D  &\approx \SI{0.948}{\second}.
\end{align}
Pro střední motor bylo obdobně zjištěno
\begin{align}
g    &\approx \SI{1360}{\degree/\second}, \\
\tau &\approx \SI{0.0337}{\second}, \\
T_D  &\approx \SI{0.786}{\second}.
\end{align}

Dosazením konstant do rovnice \ref{eq:motor:h_varphi} vznikne
matematický model obou motorů v~podobě přenosové funkce.

\subsection{Návrh PID regulátoru} \label{sec:piddesign}

Pro realizaci PID regulátoru bylo zvoleno standardní paralelní
zapojení s~dvěma úpravami, viz obrázek \ref{fig:motor-pid-internals}.

\begin{figure}[ht]
\includegraphics[width=0.6\textwidth]{img/pid-internals.eps}
\caption[Zapojení PID regulátoru]{Vnitřní zapojení PID regulátoru.}
\label{fig:motor-pid-internals}
\end{figure}

První úprava spočívá v~použití vnějšího signálu jako derivace.
Jiná zapojení mívají derivační složku počítanou přímo z~regulační
odchylky $e$ za pomocí filtrované derivace. Zde bylo, obdobně jako PXMC \cite{pxmc},
využito rozdílu známé rychlosti motoru a plánované rychlosti na trajektorii.

Druhou úpravu představuje zapojení vnitřní saturace a \uv{anti-windup} obvodu.
Jejich cílem je zmenšit překmit motoru, pokud by regulátor
po určitou dobu požadoval příliš vysoký výkon motorů. Po tuto dobu
by se na integrátoru akumulovala odchylka, která by se pak musela
v~překmitu od-integrovat. \cite[655]{franklin}

Anti-windup zde byl realizován pomocí logiky. Pokud výstupní signál
překročí nastavenou mez, regulátor jej omezí na přípustnou úroveň.
Zároveň ale upraví stav integrátoru tak, aby nový výstupní signál
celého PID regulátoru byl přesně na hranici saturace.
Tím je zajištěno, že integrátor nevytvoří příliš velký překmit.

Za vzorkovací periodu regulátoru byl zvolen čas $T_\text{s} = \SI{10}{\milli\second}$.
S~odpovídající frekvencí přichází informace o~rychlosti motoru.

Hodnotu frekvence v~současnosti nelze přenastavit bez změny zdrojového
kódu Open-Cube. Regulátor lze ale obejít a ovládat motory z~uživatelské
aplikace přímo přes střídu PWM.

Konstanty regulátoru $K_\text{P}$, $K_\text{I}$ a $K_\text{D}$ byly navrženy
opět pomocí prostředí Matlab. Získaný spojitý model motoru byl diskretizován
metodou ZOH (zero-order hold) se vzorkovací periodou $T_\text{s}$.
Následně byl využit \uv{PID tuner} \cite{pidtuner}, který dokáže na základě požadavků navrhnout
příslušné konstanty. Jako požadavek byla zvolena šířka pásma otevřené smyčky $\omega_\text{c}$.

Postupnou iterací bylo dosaženo hodnoty $\omega_\text{c} = \SI{20}{\radian\per\second}$.
Při té již byla odezva motoru dostatečně rychlá. Regulátor ale stále zvládal
potlačovat šum v~měřené rychlosti popsaný v~sekci \ref{sec:motors:speed:realization}.

\noindent Po přeškálování konstant pro výstup v~\% střídy PWM byly pro velký motor získány konstanty
\begin{align}
K_\text{P} &= \SI{3.11}{}, \\
K_\text{I} &= \SI{6.22}{}, \\
K_\text{D} &= \SI{0.135}{}.
\end{align}
Pro střední motor stejným postupem vyšlo
\begin{align}
K_\text{P} &= \SI{1.56}{}, \\
K_\text{I} &= \SI{4.75}{}, \\
K_\text{D} &= \SI{0.0666}{}.
\end{align}

Obrázek \ref{fig:mreg-cmp} ukazuje odezvu modelů na jednotkový skok
po zapojení regulátoru.
\begin{figure}[ht]
\includegraphics[width=\textwidth]{img/reg-comparison.eps}
\caption[Odezva modelů na skok reference]{Odezva regulovaných modelů motorů na skok referenčního úhlu.}
\label{fig:mreg-cmp}
\end{figure}


\subsection{Generování trajektorie}
Zbývající částí řídící smyčky je generátor referenční trajektorie.
Pro potřeby Open-Cube stačí vytvářet trajektorie s~lichoběžníkovým
průběhem rychosti, viz obrázek \ref{fig:ramp}. Podobné průběhy používá
například leJOS EV3 \cite{lejos-ramp}.
\begin{figure}[ht]
\includegraphics[width=0.8\textwidth]{img/ramp.eps}
\caption{Ukázka možného profilu rychosti.}
\label{fig:ramp}
\end{figure}

Generátor dokáže vytvářet trajektorie omezené dobou trvání pohybu nebo cílovým úhlem.
Dokáže také vytvářet neomezené trajektorie (motor se bez zásahu
uživatele nezastaví). Implementován byl jako stavový automat
znázorněný na obrázku \ref{fig:ramp-fsm}.
\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{img/ramp-fsm.pdf}
\caption[Stavový automat gen. reference]{Stavový automat generátoru reference.}
\label{fig:ramp-fsm}
\end{figure}

\pagebreak

\subsubsection{Fáze zrychlování}
V~této fázi motor postupně zrychluje z~počáteční (ne nutně nulové)
rychlosti $\omega_0$ na cílovou rychlost $\omega_\text{target}$ se zrychlením $a$.
Trajektorie je zde až na znaménka definovaná rovnicemi
\begin{align}
\varphi_r(t) &= \varphi_0 + \omega_0 (t-t_0) + \frac{1}{2}a(t-t_0)^2, \\
\omega_r(t)  &= \omega_0 + a(t-t_0),
\end{align}
kde $\varphi_0$ a $t_0$ jsou rychlost motoru a čas na začátku manévru.

Tato fáze trvá pouze dobu nutnou k~dosažení cílové rychlosti:
\begin{equation}
T_\text{rampup} = \left|\frac{\omega_\text{target}-\omega_0}{a}\right|.
\end{equation}
Fáze může skončit dříve, pokud je trajektorie krátká a profil rychlosti
neobsahuje fázi konstantní rychlosti. Toto se kontroluje podle úhlu nebo
času (záleží na typu pohybu), za který stihne motor ze současné
rychlosti zastavit. Kritérium je blíže popsáno u~následující fáze.

\subsubsection{Fáze konstantní rychlosti}
Zde se motor pohybuje rovnoměrně podle rovnic
\begin{align}
\varphi_r(t) &= \varphi_1 + \omega_\text{target} (t-t_1), \\
\omega_r(t)  &= \omega_\text{target},
\end{align}
kde $\varphi_1$ a $t_1$ jsou rychlost a čas na počátku této fáze.

Konec této fáze se vyhodnocuje podle typu omezení. Při omezení na
celkový čas skončí fáze v~okamžiku, kdy zbývá do konce čas
\begin{equation}
T_\text{rampdown} = \left|\frac{\omega_\text{target}}{a} \right|.
\end{equation}
Za tento čas stihne stihne motor se zrychlením $-a$ zastavit.

Při omezení na celkový úhel se fáze ukončí v~momentě, kdy motoru
v~manévru zbývá úhlová vzdálenost
\begin{equation}
\varphi_\text{rampdown} = \left| \frac{\omega_\text{target}^2}{2a} \right|.
\end{equation}
Pak zpomalující motor se zrychlením $-a$ zastaví právě na cílovém úhlu.

Pro trajektorie bez omezení je tato fáze konečná.

\subsubsection{Fáze zpomalování}
Při této fázi motor zpomaluje podle rovnic
\begin{align}
\varphi_r(t) &= \varphi_2 + \omega_2 (t-t_2) - \frac{1}{2}{\tilde a}(t-t_2)^2, \\
\omega_r(t)  &= \omega_2 - {\tilde a}(t-t_2).
\end{align}
Proměnné $\varphi_2$, $\omega_2$ a $t_2$ opět odpovídají stavu na počátku fáze.
Fáze trvá pouze čas $T_\text{rampdown}$.

Tato fáze nepoužívá přímo zadané zrychlení $a$. Pokud je trajektorie omezená
úhlem, použije se jiné zrychlení:
\begin{align}
{\tilde T_\text{rampdown}} = 2 \left| \frac{\varphi_\text{target} - \varphi_2}{\omega} \right|, \\
{\tilde a} =  \left| \frac{\omega_2}{\tilde T_\text{rampdown}} \right|.
\end{align}
Tato změna zajistí, že se motor zastaví v~cílové poloze i v~případě, že
se motor na začátku fáze otáčel mírně pomaleji.

Pro ostatní typy trajektorií platí $\tilde a = a$.

\subsubsection{Fáze zpomalování pro změnu směru}
Tato fáze se normálně neuplatní. Použije se pouze na začátku
vybraných trajektorií s~cílovým úhlem. Motor musí již být roztočený a
v~důsledku toho by nestihl se zrychlením $-a$ zastavit na správném místě.
Generátor proto nejprve motor rovnoměrně zpomaleně zastaví a až pak z~této
polohy začne sledovat lichoběžníkovou trajektorii rychlosti.

Rovnice pro tuto fázi odpovídají normální fázi zpomalování s~$\tilde a = a$.

\subsubsection{Fáze zastavení}
Tato fáze je konečná pro všechny omezené trajektorie. Generátor vrací hodnoty
\begin{align}
\varphi_r(t) &= \varphi_\text{target}, \\
\omega_r(t)  &= 0.
\end{align}

Pro trajektorie omezené dobou trvání pohybu odpovídá $\varphi_\text{target}$ poloze motoru
na konci zpomalovací fáze. Pro trajektorie omezené úhlem je to právě cílový úhel.

\vfil
\pagebreak
\section{Aplikační rozhraní}
Vytvořený řídící systém je nutné zpřístupnit uživateli.
Podobně jako u~senzorů bylo jedním z~cílů rozhraní co nejvíce přiblížit
prostředí Pybricks \cite{pybricks-ev3api}.

Veškerá funkcionalita je obsažená v~Python třídě \verb|lib.ev3.Motor|.
Její metody a atributy se dají rozdělit do čtyř skupin.
\subsection{Vytvoření a uvolnění objektu}
\begin{itemize}
  \item Konstruktor \verb|__init__(port, type, positive_direction)|
        umožňuje uživateli vytvořit novou instanci třídy.
        \begin{itemize}
            \item Parametr \verb|port| určuje, který port bude tato třída ovládat.
                  Přípustné hodnoty jsou Open-Cube konstanty \verb|Port.M1| až \verb|Port.M4|.
            \item Argument \verb|type| říká, jestli je k~portu připojený
                  velký nebo střední EV3 motor. Podle toho se zvolí
                  příslušný regulátor. Přípustné jsou hodnoty
                  \verb|Motor.LARGE_MOTOR| a \verb|Motor.MEDIUM_MOTOR|.
                  Argument není povinný, výchozí volbou je velký motor.
            \item Parametrem \verb|positive_direction| lze ovlivnit,
                  který směr otáčení motoru bude kladný.
                  Přípustné hodnoty jsou \verb|Motor.CLOCKWISE| (výchozí) a
                  \verb|Motor.COUNTERCLOCKWISE|. Volba ovlivňuje také
                  znaménka funkcí \verb|angle()| a \verb|speed()|.
        \end{itemize}
        Pro daný port může kdykoliv existovat pouze jeden aktivní objekt \verb|Motor|.
        Při pokusu o~vytvoření druhé instance se vyvolá výjimka \verb|RuntimeError|.
  \item Metoda \verb|close()| uvolní port pro použití jiným objektem.
\end{itemize}

\subsection{Nastavení regulátorů}
\begin{itemize}
  \item Atribut \verb|pid| obsahuje trojici \verb|(kp, ki, kd)|. Pomocí něj
        lze přečíst a případně změnit aktuální regulační konstanty.
  \item Atribut \verb|max_acceleration| určuje maximální zrychlení
        motoru, které generátor trajektorie použije. Hodnota je ve stupních za
        sekundu na druhou. Výchozí hodnota je \SI{6000}{\degree/\second^2},
        ta byla převzata z~leJOS EV3 \cite{lejos-motor-regulated}.
  \item Atribut \verb|max_power| určuje maximální dovolenou velikost
        napětí na svorkách motoru v~procentech napětí baterie.
\end{itemize}

\subsection{Zjištění stavu motoru}
\begin{itemize}
  \item Metoda \verb|angle()| vrátí současnou polohu motoru ve stupních.
  \item Metoda \verb|reset_angle(new_angle=0)| umožňuje přenastavit čítač
        polohy na novou hodnotu \verb|new_angle|.
  \item Metoda \verb|speed()| vrátí současnou hodnotu rychlosti motoru
        ve stupních za sekundu.
  \item Metoda \verb|power()| zpřístupňuje aktuální velikost a polaritu
        napětí na svorkách motoru. Hodnota je v~procentech napětí baterie
        Open-Cube.
  \item Metoda \verb|is_complete()| zjistí, jestli motor právě sleduje
        nějakou omezenou trajektorii (např. pomocí \verb|run_angle|).
        Pokud nikoliv, vrací funkce \verb|True|.
        Nekonečné pohyby (\verb|run()|, \verb|dc()|) se považují za skončené již od začátku,
        aby nedošlo k~nečekaným uváznutím uživatelské aplikace.
  \item Metoda \verb|wait_complete()| počká, dokud \verb|is_complete()|
        nezačne vracet \verb|True|.
\end{itemize}

\subsection{Povely pro řízení motoru}
\begin{itemize}
  \item Metoda \verb|coast()| (též dostupná pod názvem \verb|stop()|)
        přeruší sledování trajektorie a motor odbrzdí.
  \item Metoda \verb|brake()| přeruší sledování trajektorie a motor
        pasivně zabrzdí. V~tomto režimu lze s~motorem otáčet pouze ztuha.
  \item Metoda \verb|hold()| zablokuje motor v~současné pozici.
        Motor bude regulátorem aktivně držený na místě a nepůjde s~ním otáčet.
  \item Metoda \verb|dc(power)| přeruší sledování trajektorie a nastaví na vinutí motoru
        příslušné napětí v~procentech napětí baterie Open-Cube.
  \item Metoda \verb|run(speed)| plynule roztočí motor na rychlost \verb|speed|
        ve stupních za sekundu.
  \item Metoda \verb|run_angle(speed, rotation_angle, then=Motor.HOLD,| \\\verb|wait=True)|
        plynule otočí motor o~úhel \verb|rotation_angle| ve stupních. Motor se
        bude otáčet rychlostí nejvýše \verb|speed| stupňů za sekundu.

        Parametr \verb|then| určuje, jakým způsobem motor zabrzdí na konci pohybu.
        Přípustné možnosti jsou \verb|Motor.COAST|, \verb|Motor.BRAKE| a \verb|Motor.HOLD|.
        Ty přímo odpovídají voláním \verb|coast()|, \verb|brake()| a \verb|hold()|.

        Argument \verb|wait| určuje, zda metoda počká na konec manévru.
        Při použití \verb|wait=False| lze využít funkce \verb|is_complete()| a
        \verb|wait_complete()|.
  \item Metoda \verb|run_target(speed, target_angle, then=Motor.HOLD,| \\\verb|wait=True)|
        plynule natočí motor do polohy \verb|target_angle|. Zbylé argumenty
        mají stejný význam jako u~funkce \verb|run_angle()|.
  \item Metoda \verb|run_time(speed, time, then=Motor.HOLD, wait=True)|
        nechá motor plynule otáčet po celkový čas \verb|time| milisekund.
        Zbylé argumenty mají stejný význam jako u~funkce \verb|run_angle()|.
  \item Metoda \verb|track_target(target_angle, target_speed=0)| umožňuje
        uživateli sledovat vlastní trajektorii. Zavolání této metody
        aktivuje jen PID regulátor a nastaví jeho referenci na \verb|target_angle|
        pro polohu a \verb|target_speed| pro rychlost.
\end{itemize}

\section{Výsledek} \label{sec:motordiscuss}

Chování systému jako celku bylo otestováno změřením odezvy velkého EV3 motoru na povel
\verb|run_angle(360, 360)|. Maximální zrychlení bylo předem nastaveno na \SI{720}{\degree/\second^2}.

Výsledkem jsou průběhy na obrázcích \ref{fig:motor-test-pos} a \ref{fig:motor-test-speed}.
Ty zaznamenávají tři průběhy: ideální referenci vytvořenou v~prostředí Matlab,
odezvu matematického modelu a odezvu skutečného motoru.

Odezva z~pohledu polohy \ref{fig:motor-test-pos} vypadá podle očekávání. Motor referenci
sleduje poměrně dobře. Poloha má mírný překmit; to ale odpovídá
pozorováním u~návrhu PID konstant.

\begin{figure}[htb]
\includegraphics[width=0.8\textwidth]{img/motor-test-pos.eps}
\caption[Odezva polohy motoru na testovací povel]{Časový průběh úhlové polohy při provádění testovacího povelu.}
\label{fig:motor-test-pos}
\end{figure}

V~průbězích rychlosti \ref{fig:motor-test-speed} ale lze nalézt několik příležitostí pro zlepšení.
Odezva rychlosti při návrhu regulátoru nebyla nesledována, proto
se některé vlastnosti projevily až zde.

\begin{itemize}
  \item \textbf{Překmit}. Pro jeho zmenšení by bylo možné zvolit jiné
        regulační konstanty. Problém řeší také použití jiného
        nastavení PID tuneru -- při zafixování cíle na sledování reference
        se překmit v~simulacích zmenšil.
  \item \textbf{Zpoždění ve sledování reference}. Lepšího výsledku by mohlo
        jít dosáhout jinou volbou regulačních konstant.
        Regulátor by také šlo také rozšířit o~přímovazební blok od reference
        rychlosti, který by zajistil včasnou reakci motoru.
  \item \textbf{Oscilace rychlosti}. Její zdroj se nepodařilo přesně identifikovat.
        Může se jednat o~nepředvídanou interakci zvolené
        metody měření rychlosti s~regulátorem. Může také jít o~příliš velké
        zesílení otevřené smyčky na problematické frekvenci.
  \item \textbf{Proměnlivá frekvence měření rychlosti}. V~obrázku \ref{fig:motor-test-speed}
        lze pozorovat nevýhodu metody měření rychlosti \ref{sec:motors:speed}. Při nízkých
        rychlostech přichází nové výsledky měření pomaleji -- v~nejhorším případě každých
        \SI{100}{\milli\second}. Při nízkých rychlostech jsou totiž intervaly mezi pulzy
        enkodéru dlouhé a zvolená metoda má tomu úměrné zpoždění. Jedním možným řešením
        by bylo omezit min. měřitelnou rychlost na \qtyrange{50}{100}{\degree/\second}.
  \item \textbf{Chybná měření při malých rychlostech}. Na konci manévru
        je patrný chvilkový skok naměřené rychlosti na hodnotu \SI{-80}{\degree/\second}.
        Motor se ale touto rychlostí reálně nepohyboval.
        Aby regulátor na tento skok nereagoval, byla do něj přidána podmínka,
        aby se při požadavku na rychlost nižší než \SI{30}{\degree/\second}
        považovala naměřená rychlost za nulovou.

        Později bylo zjištěno, že skok je způsobený implementační chybou
        při měření malých rychlostí. Finální kód již má tuto chybu opravenou.
  \item \textbf{Vstup do generátoru trajektorie}. Ten aktuálně některá
        rozhodnutí provádí na základě okamžité polohy a rychlosti motoru.
        Jak je ale z~grafů patrné, hodnoty mohou mít určité zpoždění.
        Jedním z~nepřímých důsledků je náhlý propad rychlosti na začátku
        zpomalovací fáze. Lepším řešením by bylo generovat trajektorii
        bez zpětné vazby od motoru.
\end{itemize}


\begin{figure}[htb]
\includegraphics[width=0.8\textwidth]{img/motor-test-speed.eps}
\caption[Odezva rychlosti motoru na testovací povel]{Časový průběh úhlové rychlosti při provádění testovacího povelu.}
\label{fig:motor-test-speed}
\end{figure}
