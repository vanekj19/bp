#!/usr/bin/env python3
import argparse

from serial import Serial

from low_level_defs import INITIAL_BAUDRATE, SystemMessageType
from message_codec import decode_message, build_message
from message_defs import SystemMessage, DataMessage, SensorModeSetMessage
from rxtx import receive_loop, SensorHeartbeat, send_message
from sensor_meta import SensorMetadata, EV3_COLOR_SENSOR_ID, lego_sensors


def main():
    parser = argparse.ArgumentParser("python-ntb-sensors")
    parser.add_argument("serial_port", help="Serial port name")
    parser.add_argument("-m", "--mode", help="Mode to switch the sensor to", required=False)
    parser.add_argument("-s", "--stop", help="Exit after receiving sensor information", action='store_true')
    args = parser.parse_args()

    serial = Serial(args.serial_port, INITIAL_BAUDRATE)
    meta = SensorMetadata()
    data_mode = False
    need_mode_switch = args.mode is not None
    hb = SensorHeartbeat(serial, period=0.5)
    print("Starting...")
    for buffer in receive_loop(serial, hb):
        try:
            msg = decode_message(buffer, meta.identifier == EV3_COLOR_SENSOR_ID)
        except ValueError:
            # todo only on checksum error
            # todo limit attempts
            send_message(serial, SystemMessage(SystemMessageType.NACK))
            continue

        if not data_mode:
            print(msg)
            meta.update(msg)

            if meta.finished:
                print("ACKing typedata")
                serial.write(build_message(SystemMessage(SystemMessageType.ACK)))

            if isinstance(msg, SystemMessage) and msg.type == SystemMessageType.ACK:
                data_mode = True
                hb.enable()
                summarize_typedata(meta)
                if args.stop:
                    print("Sensor metadata received, exiting.")
                    break
                print(f"Switching baudrate: {meta.baud_rate}")
                serial.baudrate = meta.baud_rate

        else:
            if not isinstance(msg, DataMessage):
                continue

            if need_mode_switch:
                need_mode_switch = False
                for i in range(meta.mode_count):
                    if meta.modes[i].name == args.mode:
                        print(f"Switching to mode {args.mode}")
                        serial.write(build_message(SensorModeSetMessage(i)))
                        break
                else:
                    raise ValueError(f"Unknown mode {args.mode}")


            mode = meta.modes[msg.mode_number]
            raw = mode.decode_data(msg.payload)
            si = mode.transform_all_to_si(raw)
            fmt_values = [f"{x:{mode.figures}.{mode.decimals}f} {mode.unit}" for x in si]
            fmt_string = ", ".join(fmt_values)
            print(f"Reading from {mode.name}: {fmt_string}")


def summarize_typedata(meta: SensorMetadata):
    if meta.identifier in lego_sensors:
        pretty_name = f" ({lego_sensors[meta.identifier]})"
    else:
        pretty_name = ""

    print()
    print("Initialization done. Sensor metadata:")
    print(f" - ID: {meta.identifier}{pretty_name}")
    print(f" - has {meta.mode_count} modes total, {meta.visible_mode_count} visible to user")
    print(f" - communicates at {meta.baud_rate} Bd")
    print(f" - mode details:")
    for idx, mode in enumerate(meta.modes):
        print(f"   * Mode {idx}: {mode.name}")
        print(f"     - returns {mode.values} value(s)")
        print(f"     - raw range: {mode.raw_limits.fmin} ~ {mode.raw_limits.fmax}")
        print(f"     - percent range: {mode.pct_limits.fmin}% ~ {mode.pct_limits.fmax}%")
        print(f"     - SI range: {mode.si_limits.fmin}{mode.unit} ~ {mode.si_limits.fmax}{mode.unit}")
        print(f"     - datatype sent from sensor: {mode.format.name}")
        print(f"     - value formatting: show {mode.figures} figures & {mode.decimals} decimals")
    print()


if __name__ == '__main__':
    main()
