import struct
from typing import Tuple

from low_level_defs import *
from message_defs import *


# Source of layouts: https://github.com/mindboards/ev3sources/blob/master/lms2012/d_uart/Linuxmod_AM1808/d_uart_mod.c

def build_message(message: Message) -> bytes:
    # system messages are single-byte
    if isinstance(message, SystemMessage):
        return bytes([encode_system_message(message.type)])

    # remaining messages are multibyte and contain a XOR checksum
    if isinstance(message, CommandMessage):
        cmd, data = build_command_payload(message)
        payload_length = round_up_payload_length(len(data))
        padded_data = data.ljust(payload_length, b'\0')
        msg_data = i2b(encode_command_header(cmd, payload_length)) + \
                   padded_data

    elif isinstance(message, InfoMessage):
        mode, subtype, data = build_info_payload(message)
        payload_length = round_up_payload_length(len(data))
        padded_data = data.ljust(payload_length, b'\0')
        msg_data = i2b(encode_info_header(mode, payload_length)) + \
                   i2b(subtype.value) + \
                   padded_data
    elif isinstance(message, DataMessage):
        payload_length = round_up_payload_length(len(message.payload))
        padded_data = message.payload.ljust(payload_length, b'\0')
        msg_data = i2b(encode_data_header(message.mode_number, payload_length)) + \
                   padded_data

    else:
        raise ValueError(f"Unknown message type: {message}")

    return msg_data + i2b(calculate_checksum(msg_data))


def build_command_payload(msg: CommandMessage) -> Tuple[CommandMessageType, bytes]:
    if isinstance(msg, SensorTypeMessage):
        cmd = CommandMessageType.TYPE
        data = i2b(msg.identifier)
    elif isinstance(msg, SensorModeCountMessage):
        cmd = CommandMessageType.MODES
        data = struct.pack("BB", msg.mode_count - 1, msg.visible_mode_count - 1)
    elif isinstance(msg, SensorSpeedMessage):
        cmd = CommandMessageType.SPEED
        data = struct.pack("<I", msg.baudrate)
    elif isinstance(msg, SensorModeSetMessage):
        cmd = CommandMessageType.SELECT
        data = struct.pack("B", msg.mode_number)
    elif isinstance(msg, SensorRawWriteMessage):
        cmd = CommandMessageType.WRITE
        data = msg.payload
    else:
        raise ValueError(f"Unknown command type: {msg}")
    return cmd, data


def build_info_payload(msg: InfoMessage) -> Tuple[int, InfoMessageType, bytes]:
    if isinstance(msg, ModeNameMessage):
        cmd = InfoMessageType.NAME
        data = msg.mode_name.encode('ascii')
    elif isinstance(msg, ModeLimitsMessage):
        if msg.limit_type == LimitType.RAW:
            cmd = InfoMessageType.RAW
        elif msg.limit_type == LimitType.PCT:
            cmd = InfoMessageType.PCT
        elif msg.limit_type == LimitType.SI:
            cmd = InfoMessageType.SI
        else:
            raise ValueError(f"Unknown value limit type: {msg.limit_type}")
        data = struct.pack("<ff", msg.min_bound, msg.max_bound)
    elif isinstance(msg, ModeUnitMessage):
        cmd = InfoMessageType.SYMBOL
        data = msg.mode_si_symbol.encode('ascii').ljust(8, b' ')[0:8]
    elif isinstance(msg, ModeFormatMessage):
        cmd = InfoMessageType.FORMAT
        data = struct.pack(
            "BBBB",
            msg.number_of_values,
            msg.format_of_values.value,
            msg.show_figures,
            msg.show_decimals
        )
    else:
        raise ValueError(f"Unknown info type: {msg}")
    return msg.mode_number, cmd, data


def decode_message(from_sensor: bytes, is_color_sensor: bool = False) -> Message:
    msg_type = decode_message_type(from_sensor[0])
    if msg_type == MessageType.SYSTEM:
        return SystemMessage(decode_system_message(from_sensor[0]))

    just_data = from_sensor[0:-1]
    received_csum = from_sensor[-1]
    correct_csum = calculate_checksum(just_data)

    if msg_type == MessageType.COMMAND:
        require_csum(received_csum, correct_csum)
        return decode_command(just_data)
    elif msg_type == MessageType.INFO:
        require_csum(received_csum, correct_csum)
        return decode_info(just_data)
    elif msg_type == MessageType.DATA:
        mode = extract_sensor_mode(just_data[0])
        raw_payload = just_data[1:]

        if not (is_color_sensor and mode == 4):
            # firmware bug in color sensor: checksum is not correct in RGB-RAW mode
            require_csum(received_csum, correct_csum)

        return DataMessage(mode, raw_payload)
    else:
        raise ValueError(f"Unknown received message type: {msg_type} (raw data: {from_sensor.hex()})")


def decode_command(data: bytes) -> CommandMessage:
    subtype = decode_command_type(data[0])
    if subtype == CommandMessageType.TYPE:
        _, sensor_id = struct.unpack("<BB", data)
        return SensorTypeMessage(sensor_id)
    elif subtype == CommandMessageType.MODES:
        _, num_all, num_visible = struct.unpack("<BBB", data)
        return SensorModeCountMessage(num_all + 1, num_visible + 1)
    elif subtype == CommandMessageType.SPEED:
        _, baud = struct.unpack("<BI", data)
        return SensorSpeedMessage(baud)
    elif subtype == CommandMessageType.SELECT:
        _, mode = struct.unpack("<BB", data)
        return SensorModeSetMessage(mode)
    elif subtype == CommandMessageType.WRITE:
        return SensorRawWriteMessage(data[1:])
    else:
        raise ValueError(f"Unknown command message type: {subtype}")


def decode_info(data: bytes) -> InfoMessage:
    mode = extract_sensor_mode(data[0])
    subtype = decode_info_message_type(data[1])
    if subtype == InfoMessageType.NAME:
        name = data[2:].rstrip(b'\0').decode('ascii').rstrip(' ')
        return ModeNameMessage(mode, name)
    elif subtype == InfoMessageType.RAW:
        _, _, fmin, fmax = struct.unpack("<BBff", data)
        return ModeLimitsMessage(mode, LimitType.RAW, fmin, fmax)
    elif subtype == InfoMessageType.SI:
        _, _, fmin, fmax = struct.unpack("<BBff", data)
        return ModeLimitsMessage(mode, LimitType.SI, fmin, fmax)
    elif subtype == InfoMessageType.PCT:
        _, _, fmin, fmax = struct.unpack("<BBff", data)
        return ModeLimitsMessage(mode, LimitType.PCT, fmin, fmax)
    elif subtype == InfoMessageType.SYMBOL:
        value = data[2:].rstrip(b'\0').decode('ascii').rstrip(' ')
        return ModeUnitMessage(mode, value)
    elif subtype == InfoMessageType.FORMAT:
        _, _, values, fmt, figures, decimals = struct.unpack("<BBBBBB", data)
        return ModeFormatMessage(mode, values, decode_info_format_byte(fmt), figures, decimals)
    else:
        raise ValueError(f"Unknown info message type: {subtype}")


def require_csum(received: int, correct: int):
    if received != correct:
        raise ValueError(f"Received a message with invalid checksum: {received} != {correct}")
