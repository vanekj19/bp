# Source: https://github.com/mindboards/ev3sources/blob/master/lms2012/d_uart/Linuxmod_AM1808/d_uart_mod.c
# Cross-referenced with: https://github.com/JakubVanek/lms2012-stuff/tree/master/ev3color

import math
from enum import Enum


INITIAL_BAUDRATE = 2400

class MessageType(Enum):
    SYSTEM = 0b00
    COMMAND = 0b01
    INFO = 0b10
    DATA = 0b11


class SystemMessageType(Enum):
    SYNC = 0b000
    NACK = 0b010
    ACK = 0b100
    # ESC = 0b110 # reserved, but not supported


class CommandMessageType(Enum):
    TYPE = 0b000
    MODES = 0b001
    SPEED = 0b010
    SELECT = 0b011
    WRITE = 0b100


class InfoMessageType(Enum):
    NAME = 0b00000000
    RAW = 0b00000001
    PCT = 0b00000010
    SI = 0b00000011
    SYMBOL = 0b00000100
    FORMAT = 0b10000000


class DataFormatType(Enum):
    INT_8 = 0b00
    INT_16 = 0b01
    INT_32 = 0b10
    FLOAT_32 = 0b11


MESSAGE_TYPE_MASK = 0b11000000
MESSAGE_TYPE_SHIFT = 6

MESSAGE_LENGTH_MASK = 0b00111000
MESSAGE_LENGTH_SHIFT = 3
MESSAGE_LENGTH_MAX_LOG2 = 0b111
MESSAGE_LENGTH_MAX_BYTES = 2 ** MESSAGE_LENGTH_MAX_LOG2

MESSAGE_SUBTYPE_MASK = 0b00000111
MESSAGE_SUBTYPE_SHIFT = 0


def decode_payload_length(initial_byte: int) -> int:
    length_exponent = (initial_byte & MESSAGE_LENGTH_MASK) >> MESSAGE_LENGTH_SHIFT
    return 2 ** length_exponent


def round_up_payload_length(min_payload: int) -> int:
    if min_payload < 1:
        min_payload = 1
    length_exponent = math.ceil(math.log2(min_payload))
    if length_exponent > MESSAGE_LENGTH_MAX_LOG2:
        raise ValueError("Message is too long -- this protocol supports max. 128 bytes of payload")
    return 2 ** length_exponent


def encode_payload_length(payload_length: int) -> int:
    if payload_length < 1:
        payload_length = 1
    length_exponent = math.ceil(math.log2(payload_length))
    if length_exponent > MESSAGE_LENGTH_MAX_LOG2:
        raise ValueError("Message is too long -- this protocol supports max. 128 bytes of payload")
    return length_exponent << MESSAGE_LENGTH_SHIFT


def decode_message_type(initial_byte: int) -> MessageType:
    code = (initial_byte & MESSAGE_TYPE_MASK) >> MESSAGE_TYPE_SHIFT
    for message_type in MessageType:
        if message_type.value == code:
            return message_type
    else:
        raise ValueError("Unknown type of message")


def decode_system_message(initial_byte: int) -> SystemMessageType:
    code = (initial_byte & MESSAGE_SUBTYPE_MASK) >> MESSAGE_SUBTYPE_SHIFT
    for message_type in SystemMessageType:
        if message_type.value == code:
            return message_type
    else:
        raise ValueError("Unknown SYS message")


def encode_system_message(subtype: SystemMessageType) -> int:
    return subtype.value << MESSAGE_SUBTYPE_SHIFT


def decode_command_type(initial_byte: int) -> CommandMessageType:
    code = (initial_byte & MESSAGE_SUBTYPE_MASK) >> MESSAGE_SUBTYPE_SHIFT
    for message_type in CommandMessageType:
        if message_type.value == code:
            return message_type
    else:
        raise ValueError("Unknown CMD message")


def encode_command_header(subtype: CommandMessageType, payload_len: int) -> int:
    return (MessageType.COMMAND.value << MESSAGE_TYPE_SHIFT) | \
           (subtype.value << MESSAGE_SUBTYPE_SHIFT) | \
           encode_payload_length(payload_len)


def decode_info_message_type(second_byte: int) -> InfoMessageType:
    for message_type in InfoMessageType:
        if message_type.value == second_byte:
            return message_type
    else:
        raise ValueError("Unknown INFO message")


def encode_info_header(mode: int, payload_len: int) -> int:
    return (MessageType.INFO.value << MESSAGE_TYPE_SHIFT) | \
           (mode << MESSAGE_SUBTYPE_SHIFT) | \
           encode_payload_length(payload_len)


def encode_data_header(mode: int, payload_len: int) -> int:
    return (MessageType.DATA.value << MESSAGE_TYPE_SHIFT) | \
           (mode << MESSAGE_SUBTYPE_SHIFT) | \
           encode_payload_length(payload_len)


def decode_info_format_byte(fmt_byte: int) -> DataFormatType:
    for message_type in DataFormatType:
        if message_type.value == fmt_byte:
            return message_type
    else:
        raise ValueError("Unknown INFO FORMAT data type")


def extract_sensor_mode(initial_byte: int) -> int:
    code = (initial_byte & MESSAGE_SUBTYPE_MASK) >> MESSAGE_SUBTYPE_SHIFT
    return code


def calculate_checksum(data: bytes) -> int:
    csum = 0xFF
    for byte in data:
        csum = csum ^ byte
    return csum


def i2b(number: int) -> bytes:
    return bytes([number])


def b2i(array: bytes) -> int:
    return array[0]


def total_read_length(initial_byte: int) -> int:
    if decode_message_type(initial_byte) == MessageType.SYSTEM:
        return 1
    payload_len = decode_payload_length(initial_byte)
    if decode_message_type(initial_byte) == MessageType.INFO:
        return payload_len + 3  # initial byte + info type + xor checksum at the end
    else:
        return payload_len + 2  # initial byte + xor checksum at the end
