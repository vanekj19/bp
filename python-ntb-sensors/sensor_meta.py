import struct
from typing import List

from low_level_defs import DataFormatType
from message_defs import InfoMessage, SensorTypeMessage, SensorModeCountMessage, SensorSpeedMessage, \
    ModeNameMessage, ModeLimitsMessage, ModeUnitMessage, ModeFormatMessage, LimitType, Message

EV3_COLOR_SENSOR_ID = 29
EV3_SONIC_SENSOR_ID = 30
EV3_GYRO_SENSOR_ID = 32

lego_sensors = {
    EV3_COLOR_SENSOR_ID: "EV3 Color sensor",
    EV3_SONIC_SENSOR_ID: "EV3 Ultrasonic sensor",
    EV3_GYRO_SENSOR_ID: "EV3 Gyro sensor",
}

lego_sensor_patches = {}


class ValueLimit:
    def __init__(self, fmin: float, fmax: float):
        self.fmin = fmin
        self.fmax = fmax

    @property
    def span(self):
        return self.fmax - self.fmin


class SensorModeMetadata:
    def __init__(self):
        self.name: str = ""
        self.si_limits = ValueLimit(0.0, 1.0)
        self.pct_limits = ValueLimit(0.0, 100.0)
        self.raw_limits = ValueLimit(0.0, 1023.0)
        self.unit: str = ""
        self.values: int = 1
        self.format: DataFormatType = DataFormatType.INT_8
        self.figures: int = 4
        self.decimals: int = 0
        self.finished: bool = False

    def decode_data(self, buffer: bytes) -> List[float]:
        result = [0.0 for _ in range(self.values)]
        for index in range(self.values):
            if self.format == DataFormatType.INT_8:
                result[index], = struct.unpack("<b", buffer[index:index + 1])
            elif self.format == DataFormatType.INT_16:
                result[index], = struct.unpack("<h", buffer[2 * index:2 * (index + 1)])
            elif self.format == DataFormatType.INT_32:
                result[index], = struct.unpack("<i", buffer[4 * index:4 * (index + 1)])
            elif self.format == DataFormatType.FLOAT_32:
                result[index], = struct.unpack("<f", buffer[4 * index:4 * (index + 1)])
        return result

    def transform_all_to_si(self, raw: List[float]) -> List[float]:
        return [self.transform_to_si(x) for x in raw]

    def transform_all_to_pct(self, raw: List[float]) -> List[float]:
        return [self.transform_to_pct(x) for x in raw]

    def transform_to_si(self, raw: float) -> float:
        relative = (raw - self.raw_limits.fmin) / self.raw_limits.span
        return relative * self.si_limits.span + self.si_limits.fmin

    def transform_to_pct(self, raw: float) -> float:
        relative = (raw - self.raw_limits.fmin) / self.raw_limits.span
        return relative * self.pct_limits.span + self.pct_limits.fmin

    def update(self, msg: InfoMessage):
        if isinstance(msg, ModeNameMessage):
            self.name = msg.mode_name
        elif isinstance(msg, ModeLimitsMessage):
            if msg.limit_type == LimitType.PCT:
                limit = self.pct_limits
            elif msg.limit_type == LimitType.SI:
                limit = self.si_limits
            elif msg.limit_type == LimitType.RAW:
                limit = self.raw_limits
            else:
                return
            limit.fmin = msg.min_bound
            limit.fmax = msg.max_bound
        elif isinstance(msg, ModeUnitMessage):
            self.unit = msg.mode_si_symbol
        elif isinstance(msg, ModeFormatMessage):
            self.values = msg.number_of_values
            self.format = msg.format_of_values
            self.figures = msg.show_figures
            self.decimals = msg.show_decimals
            self.finished = True


class SensorMetadata:
    def __init__(self):
        self.identifier: int = -1
        self.mode_count: int = 1
        self.visible_mode_count: int = 1
        self.baud_rate: int = 2400
        self.modes: List[SensorModeMetadata] = []

    def update(self, msg: Message):
        if isinstance(msg, SensorTypeMessage):
            self.identifier = msg.identifier
        elif isinstance(msg, SensorModeCountMessage):
            self.mode_count = msg.mode_count
            self.visible_mode_count = msg.visible_mode_count
            self.modes = [SensorModeMetadata() for i in range(self.mode_count)]
        elif isinstance(msg, SensorSpeedMessage):
            self.baud_rate = msg.baudrate
        elif isinstance(msg, InfoMessage):
            self.modes[msg.mode_number].update(msg)

        if self.finished:
            if self.identifier in lego_sensor_patches:
                lego_sensor_patches[self.identifier](self)

    @property
    def finished(self):
        return len(self.modes) > 0 and all(x.finished for x in self.modes)
