import abc
from dataclasses import dataclass
from enum import Enum

from low_level_defs import SystemMessageType, DataFormatType


class Message(abc.ABC):
    pass


@dataclass
class SystemMessage(Message):
    type: SystemMessageType


class CommandMessage(Message):
    pass


@dataclass
class SensorTypeMessage(CommandMessage):
    identifier: int


@dataclass
class SensorModeCountMessage(CommandMessage):
    mode_count: int
    visible_mode_count: int


@dataclass
class SensorSpeedMessage(CommandMessage):
    baudrate: int


@dataclass
class SensorModeSetMessage(CommandMessage):
    mode_number: int


@dataclass
class SensorRawWriteMessage(CommandMessage):
    payload: bytes


@dataclass
class InfoMessage(Message):
    mode_number: int


@dataclass
class ModeNameMessage(InfoMessage):
    mode_name: str


class LimitType(Enum):
    RAW = 0
    PCT = 1
    SI = 2


@dataclass
class ModeLimitsMessage(InfoMessage):
    limit_type: LimitType
    min_bound: float
    max_bound: float


@dataclass
class ModeUnitMessage(InfoMessage):
    mode_si_symbol: str


@dataclass
class ModeFormatMessage(InfoMessage):
    number_of_values: int
    format_of_values: DataFormatType
    show_figures: int
    show_decimals: int


@dataclass
class DataMessage(Message):
    mode_number: int
    payload: bytes
