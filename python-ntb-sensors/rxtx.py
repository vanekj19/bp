import time
from typing import Optional

from serial import Serial

from low_level_defs import total_read_length, encode_command_header, CommandMessageType, calculate_checksum, \
    SystemMessageType
from message_codec import build_message
from message_defs import Message, SystemMessage

CMD_TYPE_HEADER = encode_command_header(CommandMessageType.TYPE, 1)


def frame_assembler():
    last_buffer = None
    while True:
        header = (yield last_buffer)
        total_len = total_read_length(header)
        rx_buffer = bytearray(total_len)
        rx_buffer[0] = header
        for i in range(1, total_len):
            rx_buffer[i] = (yield)

        last_buffer = rx_buffer


def wait_for_initial_packet(serial: Serial) -> bytes:
    # Technique inspired by https://github.com/mindboards/ev3sources/blob/78ebaf5b6f8fe31cc17aa5dce0f8e4916a4fc072/lms2012/d_uart/Linuxmod_AM1808/d_uart_mod.c#L2367
    # wait for CMD TYPE message - first message sent by sensor
    window = serial.read(3)

    while not is_initial_packet(window):
        next_byte = serial.read(1)
        window = window[1:] + next_byte

    return window


def is_initial_packet(data: bytes) -> bool:
    return data[0] == CMD_TYPE_HEADER and calculate_checksum(data[0:2]) == data[2]


def receive_loop(serial: Serial, hb: 'SensorHeartbeat'):
    yield wait_for_initial_packet(serial)

    assembler = frame_assembler()
    _ = next(assembler)  # consume initial None
    while True:
        remaining_time = hb.remaining_secs
        if remaining_time is not None:
            serial.timeout = remaining_time
        else:
            serial.timeout = None

        next_bytes = serial.read(1)

        if hb.is_it_time:
            hb.send_heartbeat()

        if len(next_bytes) == 0:
            continue

        opt_msg = assembler.send(next_bytes[0])
        if opt_msg is not None:
            yield opt_msg


def send_message(serial: Serial, msg: Message):
    raw = build_message(msg)
    serial.write(raw)


class SensorHeartbeat:
    def __init__(self, serial: Serial, period: float = 0.5):
        self.serial = serial
        self.enabled = False
        self.last_sent_at = 0
        self.period = period

    def enable(self):
        self.enabled = True
        self.last_sent_at = time.time()

    @property
    def is_it_time(self) -> bool:
        remaining = self.remaining_secs
        return remaining is not None and remaining == 0.0

    @property
    def remaining_secs(self) -> Optional[float]:
        if self.enabled:
            elapsed_time = time.time() - self.last_sent_at
            return max(self.period - elapsed_time, 0.0)
        else:
            return None

    def send_heartbeat(self):
        print("Sending NACK")
        self.last_sent_at = time.time()
        send_message(self.serial, SystemMessage(SystemMessageType.NACK))
