clear variables;
close all;

Ts = 0.01;
Td = 0.01;
s = tf('s');
z = tf('z', Ts);

lspds   = 844.97/100/(s*0.059163 + 1);

lGs   = 844.97/100/(s*0.059163 + 1)/s;
lGs_d = lGs * exp(-s*Td);
lG    = c2d(lGs, Ts, 'zoh');
lG_d  = c2d(lGs_d, Ts, 'zoh');

[lC, lInfo] = pidtune(lG, 'PID', 20);
lSys = feedback(lC*lG,1);
lSys_d = feedback(lC*lG_d,1);

figure;
step(lSys);
hold on;
step(lSys_d);
legend(["orig", "delayed"]);
