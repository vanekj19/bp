syms Radc Gadc Badc;

r_scale = 0.258;
g_scale = 0.280;
b_scale = 0.523;
r_offset = 0.3;
g_offset = 0.8;
b_offset = 3.7;
syms r_max r_min g_max g_min b_max b_min;

% a == 1/(M-m) * 100
% b == m/(M-m) * 100

eqs = [
    r_scale == 100/(r_max-r_min);
    r_offset == 100*r_min/(r_max-r_min);
    g_scale == 100/(g_max-g_min);
    g_offset == 100*g_min/(g_max-g_min);
    b_scale == 100/(b_max-b_min);
    b_offset == 100*b_min/(b_max-b_min);
];
sol = solve(eqs, [r_max r_min g_max g_min b_max b_min]);

fprintf("R min=%.1f max=%.1f\n", double(sol.r_min), double(sol.r_max));
fprintf("G min=%.1f max=%.1f\n", double(sol.g_min), double(sol.g_max));
fprintf("B min=%.1f max=%.1f\n", double(sol.b_min), double(sol.b_max));
