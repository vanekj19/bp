clear variables;
pulses = readmatrix("/home/kuba/tmp/tacho-diag.csv");
encA = pulses(:,1);
encA = repelem(encA, 1);
encA = encA(1:length(pulses));
t = (1:length(pulses)).';
Ttotal = t(end)-t(1);

rising_edges  = [false; diff(encA) == 1];
falling_edges = [false; diff(encA) == -1];
transitions = rising_edges | falling_edges;

rising_times = t(rising_edges);
falling_times = t(falling_edges);
transition_times = t(transitions);

Ts = 10 * 1e3;

inspected_edges = transitions;
inspected_times = transition_times;
w_factor = 1;
%inspected_edges = rising_edges;
%inspected_times = rising_times;
%w_factor = 2;

num_samp = floor(Ttotal/Ts)-1;
t_analysis = (1:num_samp).' * Ts * 1e-6;
w_window   = zeros(num_samp, 1);
w_improved = zeros(num_samp, 1);


for k = 1:num_samp
    tnow = Ts*(k+1);
    tstart = tnow - Ts + 1;
    tend   = tnow;

    irc_count = sum(inspected_edges(tstart:tend));
    w_window(k) = w_factor * irc_count / Ts * 1e6;

    last_now = tnow - Ts;
    last_transition_before = max(inspected_times(inspected_times < tstart));
    last_transition_in     = max(inspected_times((inspected_times >= tstart) & (inspected_times <= tend)));

    Th_last = last_now - last_transition_before;
    Th_now  = tnow     - last_transition_in;

    T_corr = Ts + Th_last - Th_now;
    w_improved(k) = w_factor * irc_count / T_corr * 1e6;
end

%{
t_period = inspected_times(2:end) * 1e-6;
w_period = zeros(length(t_period), 0);
for k = 1:length(t_period)
    w_period(k) = w_factor * 1 / (inspected_times(k+1) - inspected_times(k)) * 1e6;
end

Tsrs = 1 * 1e3;
w_period_resamp = zeros(floor(length(t)/Tsrs), 1);
for k = 1:length(w_period_resamp)
    tnow = k*Tsrs*1e-6;
    [M, I] = max(t_period(t_period<=tnow));
    if ~isempty(I)
        w_period_resamp(k) = w_period(I);
    end
end

b = [1 2 1];
a = [1 -1.911197067426073203932901378720998764038  0.914975834801433740572917940880870446563];
w_period_filt = filter(b, a, w_period_resamp) * 0.000944691843840150748297379568185760945  ;
%}

figure;
%plot(t_analysis, w_window);
%plot(t_period, w_period);
plot(t_analysis, w_improved);
%hold on;
%plot((Tsrs:Tsrs:Ttotal)*1e-6, w_period_filt);
%plot((Tsrs:Tsrs:Ttotal)*1e-6, movmean(w_period_resamp, 100));
legend("Mixed T/F");
