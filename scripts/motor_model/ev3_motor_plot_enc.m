clear variables;
pulses = readmatrix("/home/kuba/tmp/legomotor/medium-motor-step.csv");
encA = pulses(:,1);
encB = pulses(:,2);
t = (1:length(pulses)).';

t_range = 1913000:1918000;

figure;
subplot 211;
plot(1e-6*(t_range-t_range(1)), encA(t_range));
subplot 212;
plot(1e-6*(t_range-t_range(1)), encB(t_range));
