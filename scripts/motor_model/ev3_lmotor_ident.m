clear variables;
data = readmatrix("/home/kuba/tmp/legomotor/large-motor-step-pos.csv");

all_time = data(:,1);
all_pos  = data(:,2);

start_time = 0.75;
end_time   = 1.6;

msr_time = all_time(start_time*1000000:end_time*1000000);
msr_pos  = -all_pos(start_time*1000000:end_time*1000000);

step_fn = @(K,T,D,t) (t>=D).*(K*(t-D) - K*T + K*T*exp(-(t-D)/T));
all_err_fn = @(K,T,D) step_fn(K,T,D,msr_time) - msr_pos;

opts = optimoptions(@lsqnonlin, ...
    'Algorithm', 'levenberg-marquardt', ...
    'StepTolerance', 1e-9 ...
);
params = lsqnonlin(@(x) all_err_fn(x(1), x(2), x(3)), [1357; 0.1; 0.79], [], [], opts);

Kval = params(1);
Tval = params(2);
Dval = params(3);

s = tf('s');
csys = Kval/(s*Tval + 1) * 1/s;

sim_time = 0:0.0001:0.6;
sim_pos = lsim(csys, ones(size(sim_time)), sim_time);

figure;
plot(msr_time - Dval, msr_pos, 'LineWidth', 2);
hold on;
grid on;
plot(sim_time, sim_pos, 'LineWidth', 2);
xlim([0 0.6]);
