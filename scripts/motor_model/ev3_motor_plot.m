clear variables;

start_time = 0.70;
end_time   = 1.6;
decimation = 200;

lm_data = readmatrix("/home/kuba/tmp/legomotor/large-motor-step-pos.csv");
lm_time = downsample(lm_data(start_time*1000000:end_time*1000000, 1), decimation);
lm_pos  = downsample(-lm_data(start_time*1000000:end_time*1000000, 2), decimation);
clear lm_data;


mm_data = readmatrix("/home/kuba/tmp/legomotor/medium-motor-step-pos.csv");
mm_time = downsample(mm_data(start_time*1000000:end_time*1000000, 1), decimation);
mm_pos  = downsample(-mm_data(start_time*1000000:end_time*1000000, 2), decimation);
clear mm_data;

figure('Position', [0 0 300 250]);
plot(lm_time, lm_pos, 'LineWidth', 2);
grid on;
xlim([0.9 1.6]);
xlabel("Čas [s]");
ylabel("Úhel [°]");
title("Odezva velkého motoru");
print(gcf, '-depsc', '/home/kuba/skola/vs/semestr/bp/tex/img/lmotor-step.eps');


figure('Position', [0 0 300 250]);
plot(mm_time, mm_pos, 'LineWidth', 2);
grid on;
xlim([0.7 1.4]);
xlabel("Čas [s]");
ylabel("Úhel [°]");
title("Odezva středního motoru");
print(gcf, '-depsc', '/home/kuba/skola/vs/semestr/bp/tex/img/mmotor-step.eps');

