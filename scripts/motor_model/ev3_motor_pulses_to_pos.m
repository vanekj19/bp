clear variables;
pulses = readmatrix("/home/kuba/Plocha/medium-motor-step.csv");
encA = pulses(:,1);
encB = pulses(:,2);
t = (1:length(pulses)).';
Ttotal = t(end)-t(1);

rising_edges  = [false; diff(encA) == 1];
falling_edges = [false; diff(encA) == -1];


step1 = (rising_edges & encB) * (-1);
step2 = (rising_edges & ~encB) * (+1);
step3 = (falling_edges & encB) * (+1);
step4 = (falling_edges & ~encB) * (-1);
steps = step1 + step2 + step3 + step4;

realT = t * 1e-6;
position = cumsum(steps);
data_out = [realT, position];
writematrix(data_out, "/home/kuba/Plocha/medium-motor-step-pos.csv");
