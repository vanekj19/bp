clear variables;

s = tf('s');
K = 1360.2/100;
T = 0.033717;
Gc = K/(s*T + 1);

Ts = 0.01;
z = tf('z', Ts);
Gspeed = c2d(Gc, Ts, 'zoh');    
Gpos   = c2d(Gc/s, Ts, 'zoh');

[C,info] = pidtune(Gpos, 'PID', 20);
sys = feedback(C*Gpos,1);
