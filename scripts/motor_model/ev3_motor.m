clear variables;

Rmotor = 4.196;
Lmotor = 4.9e-3;
Upwm = 7.2;
fpwm = 20e3;


duty = 0.20;
Ts = 1e-7;
t = 0:Ts:0.01;
u = pwm_ev3_hbridge(t, duty, 1, fpwm, Upwm);

A = -Rmotor/Lmotor;
B = 1/Lmotor;
C = Rmotor;
D = 0;
sys = ss(A,B,C,D);

y = lsim(sys, u, t);

plot(t,y);
