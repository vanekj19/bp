clear variables;


% param src: "NXT Mathematical Model of Lego EV3 Motor"
R = 6.832749059810827;
L = 0.00494;
K = 0.459965726538748;
B = 0.000726962269165;
J = 0.001502739083882;
F = 0.007776695904018;
    
Afull = [
    -B/J, +K/J;
    -K/L, -R/L
];
Bfull = [
    0; 1/L
];
Cfull = [1 0];
Dfull = 0;

sys = ss(Afull,Bfull,Cfull,Dfull);

Asimp = -(B+K^2/R)/J;
Bsimp = K/R/J;
Csimp = 1;
Dsimp = 0;
simp = ss(Asimp, Bsimp, Csimp, Dsimp);
