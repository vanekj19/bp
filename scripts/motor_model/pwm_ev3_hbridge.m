function [U] = pwm_ev3_hbridge(t, Uctrl, Uctrlmax, fpwm, Upwm)
    duty = Uctrl / Uctrlmax;

    Tpwm = 1/fpwm;
    cycle_fraction = mod(t, Tpwm) / Tpwm;
    is_on = cycle_fraction <= duty;
    U = is_on * (Upwm);
end
