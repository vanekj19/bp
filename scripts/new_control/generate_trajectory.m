function [x,v,a] = generate_trajectory(time, angle, speed, accel)
    speed = abs(speed);
    accel = abs(accel);

    ramp_time = speed/accel;
    ramp_angle = 1/2*accel*ramp_time^2;

    if angle > (2*ramp_angle)
        Trampup = ramp_time;
        Tsteady = (angle-2*ramp_angle)/speed;
        Trampdown = Trampup;
    else
        Trampup = sqrt(angle/accel);
        Tsteady = 0;
        Trampdown = Trampup;
    end

    rampup_end_angle = 1/2*accel*Trampup^2;
    steady_end_angle = rampup_end_angle + Tsteady*speed;
    rampdown_end_angle = angle;

    max_speed = accel * Trampup;

    is_rampup = time < Trampup;
    is_steady = time < (Trampup+Tsteady) & time >= Trampup;
    is_rampdown = time < (Trampup+Tsteady+Trampdown) & time >= (Trampup+Tsteady);
    is_idle = time >= (Trampup+Tsteady+Trampdown);

    x = is_rampup .* (1/2*accel*time.^2) + ...
        is_steady .* (rampup_end_angle + max_speed*(time-Trampup)) + ...
        is_rampdown .* (steady_end_angle + max_speed*(time-Trampup-Tsteady) - 1/2*accel*(time-Trampup-Tsteady).^2) + ...
        is_idle .* (rampdown_end_angle);

    v = is_rampup .* (+accel .* (time)) + ...
        is_steady .* (max_speed) + ...
        is_rampdown .* (max_speed-accel .* (time-Trampup-Tsteady));

    a = is_rampup .* (+accel) + ...
        is_rampdown * (-accel);
end
