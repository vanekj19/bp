clear variables;

s = tf('s');
% LARGE MOTOR
K = 844.97/100;
T = 0.059163;
% MEDIUM MOTOR
% K = 1360.2/100;
% T = 0.033717;
Gc = K/(s*T + 1);

Ts = 0.001;
Ts_spd = 0.001;

z = tf('z', Ts);
Gspeed = c2d(Gc, Ts, 'zoh');    
Gpos   = c2d(Gc/s, Ts, 'zoh');

Vbatt = 8.0;
nxt_state_model = nxt_unroller_model * Vbatt/100;
jv_state_model = ss([-1/T, 0; 1, 0], [K/T; 0], eye(2), 0);

opts = pidtuneOptions('DesignFocus', 'disturbance-rejection');
[C,info] = pidtune(Gpos, 'PID', 20, opts);

t = (0:Ts:5).';
[x, v, a] = generate_trajectory(t, 360*4, 360, 720);
traj_x = [t, round(x)];
traj_v = [t, v];
traj_a = [t, a];


system_inversion = tf(minreal(1/Gc));
Kp = C.Kp;
Ki = C.Ki;
Kd = C.Kd;
Ka = system_inversion.num{1}(1);
Kv = system_inversion.num{1}(2);
