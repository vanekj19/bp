function [sys] = nxt_unroller_model
    % param src: "NXT Mathematical Model of Lego EV3 Motor"
    R = 6.832749059810827;
    L = 0.00494;
    K = 0.459965726538748;
    B = 0.000726962269165;
    J = 0.001502739083882;
    F = 0.007776695904018;
        
    Afull = [
        -B/J, +K/J, 0;
        -K/L, -R/L, 0;
            1,   0, 0
    ];
    Bfull = [
        0; 1/L; 0
    ];
    Cfull = [
        180/pi, 0, 0;
        0, 0, 180/pi
    ];
    Dfull = 0;
    
    sys = ss(Afull,Bfull,Cfull,Dfull);
end
