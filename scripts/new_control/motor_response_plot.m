clear variables;
close all;

Ts = 0.001;
s = tf('s');
z = tf('z', Ts);
Gs = 844.97/100/(s*0.059163 + 1)/s;
Gz = c2d(Gs, Ts, 'zoh');

opts = pidtuneOptions('DesignFocus', 'disturbance-rejection');
[C,info] = pidtune(Gz, 'PID', 20, opts);

sys = (Gz*C) / (1+Gz*C);


m = readmatrix('/home/kuba/skola/vs/semestr6/bp/data/motor-test-ff-fast.csv');


t = m(:,1) / 1e6;
pos = m(:,2);
speed = m(:,3);
power = m(:,4);
[pos_sp, speed_sp, ~] = generate_trajectory(t, 360, 360, 6000);

t_model = 0:Ts:max(t);
[model_sp, ~, ~] = generate_trajectory(t_model, 360, 360, 6000);
pos_model = lsim(sys, model_sp, t_model);
speed_model = [0; diff(pos_model)] / Ts;

height = 350;
figure('Position', [100 100 1.618*height height]);
plot(t, pos_sp, 'LineWidth', 2);
hold on;
grid on;
plot(t_model, pos_model,'LineWidth', 2);
plot(t, pos, 'LineWidth', 2);
xlim([0 max(t)]);
xlabel("Čas [s]");
ylabel("Úhel [°]");
legend(["Reference", "Model", "Měření"], "Location","best");
%print(gcf, "-depsc", "/home/kuba/skola/vs/semestr/bp/tex/img/motor-test-pos.eps");

figure('Position', [100 100 1.618*height height]);
pbaspect([1.618 1 1]);
plot(t, speed_sp, 'LineWidth', 2);
hold on;
grid on;
plot(t_model, speed_model, 'LineWidth', 2);
plot(t, speed, 'LineWidth', 2);
xlim([0 max(t)]);
xlabel("Čas [s]");
ylabel("Rychlost [°/s]");
legend(["Reference", "Model", "Měření"], "Location","best");
%print(gcf, "-depsc", "/home/kuba/skola/vs/semestr/bp/tex/img/motor-test-speed.eps");
