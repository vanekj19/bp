clear variables;

s = tf('s');
K = 844.97/100;
T = 0.059163;
Gc = K/(s*T + 1);

Ts = 0.01;
z = tf('z', Ts);
Gspeed = c2d(Gc, Ts, 'zoh');    
Gpos   = c2d(Gc/s, Ts, 'zoh');

opts = pidtuneOptions('DesignFocus', 'disturbance-rejection');
[C,info] = pidtune(Gpos, 'PID', 30, opts);
sys = feedback(C*Gpos,1);

d_spd = minreal(Gspeed/(1+C*Gpos));
d_pos = minreal(Gpos/(1+C*Gpos));
d_pwr = minreal(-Gpos*C/(1+C*Gpos));
