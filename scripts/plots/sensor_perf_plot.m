clear variables;

c_ts = readmatrix('c-ts.txt');
py_ts = readmatrix('py-ts-1khz.txt');

c_ts = (c_ts) / 1e3;
py_ts = (py_ts) / 1e3;

edges = 1.0:0.100:5.0;

figure('Position', [100 100 600 300]);
t = tiledlayout(1,2, 'TileSpacing','Compact','Padding','Compact');
nexttile;
histogram(c_ts, edges);
ylim([0 4096]);
title("Histogram pro nativní řešení");
xlabel("Doba trvání smyčky [ms]");
ylabel("Počet výskytů");

c_sd = std(c_ts);
c_err = c_sd / sqrt(length(c_ts));

nexttile;
histogram(py_ts, edges);
ylim([0 4096]);
title("Histogram pro řešení v Pythonu");
xlabel("Doba trvání smyčky [ms]");
ylabel("Počet výskytů");
print(gcf, '-depsc', '/home/kuba/skola/vs/semestr/bp/tex/img/tsbench.eps');

py_sd = std(py_ts);
py_err = py_sd / sqrt(length(py_ts));
