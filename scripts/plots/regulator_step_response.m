clear variables;
close all;

Ts = 0.01;
s = tf('s');

mG = c2d(1360.2/100/(s*0.033717 + 1)/s, Ts, 'zoh');
mC = pidtune(mG, 'PID', 20);
mSys = feedback(mC*mG,1);
mSysHeh = minreal(feedback(mC,mG));

lG = c2d(844.97/100/(s*0.059163 + 1)/s, Ts, 'zoh');
lC = pidtune(lG, 'PID', 20);
lSys = feedback(lC*lG,1);
lSysHeh = feedback(lC,lG);

Tend = 1;
[lPos, lTime] = step(lSys, Tend);
[mPos, mTime] = step(mSys, Tend);



figure('Position', [100 100 600 250]);
t = tiledlayout(1,2, 'TileSpacing','Compact','Padding','Compact');
nexttile;
plot(mTime, mPos, 'LineWidth', 2);
grid on;
xticks(0:0.2:1);
title("Odezva středního motoru");
xlabel("Čas [s]");
ylabel("Úhel [°]");

nexttile;
plot(lTime, lPos, 'LineWidth', 2);
grid on;
xticks(0:0.2:1);
title("Odezva velkého motoru");
xlabel("Čas [s]");
ylabel("Úhel [°]");

print(gcf, "-depsc", "/home/kuba/skola/vs/semestr/bp/tex/img/reg-comparison.eps");
