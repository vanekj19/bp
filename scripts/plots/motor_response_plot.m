clear variables;
close all;

Ts = 0.01;
s = tf('s');
Gs = 844.97/100/(s*0.059163 + 1)/s;
Gz = c2d(Gs, Ts, 'zoh');

%opts = pidtuneOptions('DesignFocus', 'reference-tracking');
[C,info] = pidtune(Gz, 'PID', 20);
sys = feedback(C*Gz,1);


m = readmatrix('/home/kuba/skola/vs/semestr6/bp/data/motor-test.csv');


t = m(:,1) / 1e6;
pos = m(:,2);
speed = m(:,3);
power = m(:,4);
pos_sp = phi_sp_gen(t);
speed_sp = omega_sp_gen(t);

t_model = 0:Ts:max(t);
pos_model = lsim(sys, phi_sp_gen(t_model), t_model);
speed_model = [0; diff(pos_model)] / Ts;

height = 350;
figure('Position', [100 100 1.618*height height]);
plot(t, pos_sp, 'LineWidth', 2);
hold on;
grid on;
plot(t_model, pos_model,'LineWidth', 2);
plot(t, pos, 'LineWidth', 2);
xlim([0 max(t)]);
xlabel("Čas [s]");
ylabel("Úhel [°]");
legend(["Reference", "Model", "Měření"], "Location","best");
print(gcf, "-depsc", "/home/kuba/skola/vs/semestr/bp/tex/img/motor-test-pos.eps");

figure('Position', [100 100 1.618*height height]);
pbaspect([1.618 1 1]);
plot(t, speed_sp, 'LineWidth', 2);
hold on;
grid on;
plot(t_model, speed_model, 'LineWidth', 2);
plot(t, speed, 'LineWidth', 2);
xlim([0 max(t)]);
xlabel("Čas [s]");
ylabel("Rychlost [°/s]");
legend(["Reference", "Model", "Měření"], "Location","best");
print(gcf, "-depsc", "/home/kuba/skola/vs/semestr/bp/tex/img/motor-test-speed.eps");


function [w] = omega_sp_gen(t)
    is_rampup = t <= 0.5;
    is_steady = t > 0.5 & t <= 1;
    is_rampdown = t > 1 & t <= 1.5;
    is_idle = t > 1.5;

    w = is_rampup .* (720*t) + ...
        is_steady .* (360) + ...
        is_rampdown .* (360-720*(t-1)) + ...
        is_idle .* (0);
end

function [phi] = phi_sp_gen(t)
    is_rampup = t <= 0.5;
    is_steady = t > 0.5 & t <= 1;
    is_rampdown = t > 1 & t <= 1.5;
    is_idle = t > 1.5;

    phi = is_rampup .* (720/2*t.^2) + ...
          is_steady .* (90+360*(t-0.5)) + ...
          is_rampdown .* (270+360*(t-1)-720/2*(t-1).^2) + ...
          is_idle .* 360;
end
