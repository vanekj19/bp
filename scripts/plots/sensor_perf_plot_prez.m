clear variables;

c_ts = readmatrix('/home/kuba/skola/vs/semestr/bp/data/sensor-perf/c-ts.txt');
py_ts = readmatrix('/home/kuba/skola/vs/semestr/bp/data/sensor-perf/py-ts-1khz.txt');

c_ts = (c_ts) / 1e3;
py_ts = (py_ts) / 1e3;

edges = 1.0:0.100:5.0;


py_N = histcounts(py_ts, edges);
c_N = histcounts(c_ts, edges);

figure('Position', [100 100 400 350]);
histogram(py_ts, edges, 'Normalization', 'probability');
hold on;
grid on;
histogram(c_ts, edges, 'Normalization', 'probability');

title("Stálost časování programu");
xlabel("Doba trvání smyčky [ms]");
ylabel("Relativní četnost");
legend(["Vývojová verze", "Finální verze"]);
print(gcf, '-dsvg', '/home/kuba/skola/vs/semestr/bp/tex/img/tsbench-prez.svg');
