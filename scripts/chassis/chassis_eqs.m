clear variables;

syms v_mid v_left v_right;
syms w_left w_right w_robot;
syms trackwidth;
syms wheeldiameter;
syms turningradius;
syms curvature;

eqs = [
    w_robot == v_mid / turningradius;
    w_robot == v_left / (turningradius - trackwidth/2);
    w_robot == v_right / (turningradius + trackwidth/2);
    v_left  == w_left * wheeldiameter/2;
    v_right == w_right * wheeldiameter/2;
    curvature == 1/turningradius;
];

sol_turnradius = solve(eqs, [w_left, w_right v_right, v_left, turningradius, w_robot]);
sol_wrobot = solve(eqs, [w_left, w_right v_right, v_left, turningradius, curvature]);
