clear variables;

syms v_mid v_left v_right;
syms w_left w_right w_robot;
syms trackwidth;
syms wheeldiameter;
syms turningradius;
syms curvature;
syms ang_robot ang_left ang_right dist_left dist_right;

eqs = [
    w_robot == v_left / (turningradius - trackwidth/2);
    w_robot == v_right / (turningradius + trackwidth/2);
    v_left  == w_left * wheeldiameter/2;
    v_right == w_right * wheeldiameter/2;
    ang_robot == dist_left / (turningradius - trackwidth/2);
    ang_robot == dist_right / (turningradius + trackwidth/2);
    dist_left == ang_left * wheeldiameter/2;
    dist_right == ang_right * wheeldiameter/2;    
];

sol = solve(eqs, [w_left, w_right v_right, v_left, ang_left, ang_right, dist_left, dist_right]);
soln = subs(sol, [wheeldiameter, trackwidth], [43.2, 130]);
