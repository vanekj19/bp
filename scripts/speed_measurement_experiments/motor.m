function [pos] = motor(t, w)
    angle = cumtrapz(t, w);

    miniphase = mod(angle, 2);
    full_pings = angle - miniphase;

    modded_miniphase = miniphase * 0.50/0.52;

    pos = round(full_pings) + floor(modded_miniphase);
end
