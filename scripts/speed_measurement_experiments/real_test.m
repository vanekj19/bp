clear variables; clc;

fprintf("loading data\n");
data = readmatrix("/home/kuba/tmp/legomotor/large-motor-step-pos.csv");

time_hr = data(:,1).';
time_hr = time_hr(1:5e6);
pos_hr  = data(:,2).';

Ts_oc = 1e-3;
Ts = 1e-3;

time = time_hr(1):Ts:time_hr(end);
time_oc = time_hr(1):Ts_oc:time_hr(end);
pos  = pos_hr(round(time*1e6));

fprintf("running ev3dev speed msr\n");
speed_ev3 = speedest_ev3dev      (time, time_hr, pos_hr, 20e-3);
fprintf("running opencube speed msr\n");
speed_oc  = speedest_opencube_real(time_oc, time_hr, pos_hr);
fprintf("running windowed speed msr\n");
speed_fix = speedest_fixed       (time, pos, 20e-3);
fprintf("averaging windowed speed msr\n");
speed_flt = movmean(speed_fix, [10 0]);
%speed_wma = ewma(speed_fix, 0.1);

fprintf("plotting\n");
figure;
stairs(time, speed_ev3);
grid on;
hold on;
stairs(time_oc, movmean(speed_oc,[15,0]));
%stairs(time, speed_flt);
%plot(time, speed_wma);
legend(["ev3dev", "movmean oc"], 'Location', 'southeast');
