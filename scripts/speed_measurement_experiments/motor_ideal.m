function [pos] = motor_ideal(t, w)
    pos = cumtrapz(t, w);
end
