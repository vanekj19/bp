function [speed] = speedest_opencube_real(time_sample, time_hr, pos_hr)
    ts_hr = time_hr(2) - time_hr(1);
    ts_sample = time_sample(2) - time_sample(1);
    oversampling = ts_sample / ts_hr;

    fprintf("- initializing\n");
    speed = zeros(size(time_sample));

    jump_vals = zeros(size(pos_hr));
    jump_vals(2:end) = diff(pos_hr);
    jumps = jump_vals ~= 0 & mod(pos_hr, 2) == 0;

    fprintf("- running\n");
    ds = make_data_struct;
    tedge = time_hr(1);
    position = 0;
    for k_sample = 2:length(speed)
        fprintf("- at t = %.6f s from %.0f s\n", time_sample(k_sample), time_sample(end));
        k_hr = round((k_sample - 1) * oversampling + 1);

        idx_start = max(k_hr-1e5, 1);
        idx_end = k_hr;
        step_k = idx_start + find(jumps(idx_start:idx_end), 1, 'last') - 1;

        if ~isempty(step_k)
            tedge = time_hr(step_k);
            position = pos_hr(step_k);
        end
        tnow = time_sample(k_sample);
        [spdnow, ds] = update_speed(ds, position, tedge, tnow);
    
        if ~isnan(spdnow)
            speed(k_sample) = spdnow;
        else
            speed(k_sample) = speed(k_sample-1);
        end
    end
end


function [ds] = make_data_struct()
    ds = struct;
    ds.reference_time = 0;
    ds.reference_position = 0;
end

function [speed, ds] = update_speed(ds, position, time_of_last_edge, tnow)
    delta_pulses = position          - ds.reference_position;
    delta_time   = time_of_last_edge - ds.reference_time;

    if delta_pulses ~= 0
        % got some data, calculate speed
        speed = delta_pulses / delta_time;
        ds.reference_position = position;
        ds.reference_time     = time_of_last_edge;
    elseif delta_time < 0.1
        % copy last speed, still might be nonzero
        speed = NaN;
    else
        % speed too low, set to zero
        speed = 0;
        ds.reference_position = position;
        ds.reference_time     = tnow;
    end
end
