function [speed] = speedest_dumb(time, pos, Twin)
    Ts = time(2) - time(1);
    shift = Twin/Ts;

    sample_points = 1:shift:length(time);

    pos_subset = pos(sample_points);

    pos_delta = zeros(size(pos_subset));
    pos_delta(2:end) = diff(pos_subset);
    speed = pos_delta / Twin;

    duplicated = repelem(speed, shift);
    speed = duplicated(1:length(pos));
end
