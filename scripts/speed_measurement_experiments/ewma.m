function [filtered] = ewma(raw, w)
    b = w;
    a = [1, w-1];
    filtered = filter(b,a,raw);
end
