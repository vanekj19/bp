function [speed] = speedest_fixed(time, pos, Twin)
    Ts = time(2)-time(1);
    shift = Twin/Ts;

    pos_delta = pos(1+shift:end) - pos(1:end-shift);

    speed = zeros(size(pos));
    speed(1) = 0;
    speed(2:shift) = (pos(2:shift) - pos(1)) ./ (time(2:shift) - time(1));
    speed(shift+1:end) = pos_delta / Twin;
end
