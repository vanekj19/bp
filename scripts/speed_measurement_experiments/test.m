clear variables; clc;

Ts_oc = 1e-3;
Ts = 1e-3;
Tres = 1e-5;
Tmax = 1;

w0 = 360;
wa = 360;


time    = Tres:Ts:Tmax;
time_oc = Tres:Ts_oc:Tmax;
time_hr = Tres:Tres:Tmax;
w    = time*wa+w0;
w_hr = time_hr*wa+w0;
w_oc = time_oc*wa+w0;

pos = motor(time, w);
pos_hr = motor(time_hr, w_hr);

speed_ev3 = speedest_ev3dev      (time, time_hr, pos_hr, 20e-3);
speed_oc  = speedest_opencube_real(time_oc, time_hr, pos_hr);
speed_fix = speedest_fixed       (time, pos, 20e-3);
speed_flt = movmean(speed_fix, [10 0]);
%speed_wma = ewma(speed_fix, 0.1);

figure;
plot(time, w);
grid on;
hold on;
stairs(time, speed_ev3);
stairs(time_oc, movmean(speed_oc,[9,0]));
stairs(time_oc, ewma(speed_oc,0.4));
%plot(time, speed_wma);
legend(["actual", "ev3dev", "movmean oc", "ewma oc"], 'Location', 'southeast');
