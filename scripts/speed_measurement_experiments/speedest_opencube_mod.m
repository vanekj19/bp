function [speed] = speedest_opencube_mod(time_sample, time_hr, pos_hr)
    ts_hr = time_hr(2) - time_hr(1);
    ts_sample = time_sample(2) - time_sample(1);
    oversampling = ts_sample / ts_hr;

    fprintf("- initializing\n");
    speed = zeros(size(time_sample));

    jump_vals = zeros(size(pos_hr));
    jump_vals(2:end) = diff(pos_hr);
    jumps = jump_vals ~= 0;

    fprintf("- running\n");
    for k_speed = 2:length(speed)
        fprintf("- at t = %.6f s from %.0f s\n", time_sample(k_speed), time_sample(end));
        k = round((k_speed - 1) * oversampling + 1);

        idx_start = max(k-1e5, 1);
        idx_end = k;
        step_k = idx_start + find(jumps(idx_start:idx_end), 3, 'last') - 1;

        if isempty(step_k)
            dN = 0;
            dT = 1;
        elseif length(step_k) < 3
            k1 = step_k(1);
            dN = pos_hr(k1) - pos_hr(1);
            dT = time_hr(k1) - time_hr(1);
        else
            k0 = step_k(1);
            k1 = step_k(3);
            dN = pos_hr(k1) - pos_hr(k0);
            dT = time_hr(k1) - time_hr(k0);
        end
        speed(k_speed) = dN / dT;
    end
end
