function [speed] = speedest_ev3dev(time_sample, time_hr, pos_hr, Twin_min)
    ts_hr = time_hr(2) - time_hr(1);
    ts_sample = time_sample(2) - time_sample(1);
    oversampling = ts_sample / ts_hr;

    fprintf("- initializing\n");
    speed = zeros(size(time_sample));

    jump_vals = zeros(size(pos_hr));
    jump_vals(2:end) = diff(pos_hr);
    jumps = jump_vals ~= 0;

    jump_ks = find(jumps);

    fprintf("- running\n");
    for k_speed = 2:length(speed)
        fprintf("- at t = %.6f s from %.0f s\n", time_sample(k_speed), time_sample(end));
        k = round((k_speed - 1) * oversampling + 1);

        k1 = jump_ks(find(jump_ks < k, 1, 'last'));
        if isempty(k1)
            speed(k_speed) = 0;
            continue
        end
 

        k_min = k1 - Twin_min / ts_hr;
        prev_steps = find(jump_ks < k_min, 2, 'last');
        if length(prev_steps) < 2
            k0 = 1;
        else
            k0_v1 = jump_ks(prev_steps(1));
            k0_v2 = jump_ks(prev_steps(2));
            dN_v1 = pos_hr(k1) - pos_hr(k0_v1);
            if mod(dN_v1, 2) == 0
                k0 = k0_v1;
            else
                k0 = k0_v2;
            end
        end
        dN = pos_hr(k1) - pos_hr(k0);
        dT = time_hr(k1) - time_hr(k0);
        if time_hr(k) - time_hr(k1) < 0.050
            speed(k_speed) = dN / dT;
        else
            speed(k_speed) = 0;
        end
    end
end
